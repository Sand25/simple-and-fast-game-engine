#ifndef MOUSECONTROL_H_
#define MOUSECONTROL_H_


#include <SFML/Graphics.hpp>
#include "iostream"
#include "ResourceManager.h"

using namespace sf;
using namespace std;

extern Vector2f curMousePos;

//�������� ��� �����
class MouseControl
{

	bool isPresL, downL, isPresR, downR = false;
	float mX, mY = 0;


public:

	Sprite cursor;
	Sprite pointer;
	//�����-�������������. ��� ����� ��������� ��� �������
	void init();
	//����� �����. ������� � ������� ��������� ������� (bugged)
	void mouseCheck();
	//��������� ���� ����� ������� ����. ���������� bool.
	bool mouseClickLeft();
	//��������� ���� ������ ������� ����. ���������� bool.
	bool mouseClickRight();
	//��������� ������ �� ����� ������ ����. ���������� bool.
	bool mouseDownLeft();
	//��������� ������ �� ������ ������ ����. ���������� bool.
	bool mouseDownRight();
	//��������� ����������� ����� ������ ����. ��������� ������ down. ���������� bool. 
	bool mouseUpLeft();
	//��������� ����������� ������ ������ ����. �������� ������ down. ���������� bool.
	bool mouseUpRight();
	//��������� ����������������� ��������� ����
	void mousePosCords(RenderWindow &window);


	Sprite updateCursor();

	IntRect getCursorRect();
	
	//IntRect getCursorRectWithOffset()
	//{
	//	cursor.setPosition(curMousePos);
	//	return IntRect(Vector2i(cursor.getPosition().x + offset.x, cursor.getPosition().y + offset.y), Vector2i(1, 1));
	//}

	//���������� Vector2i ������� �������
	Vector2i mousePos(RenderWindow &window);


};
#endif // !MOUSECONTROL_H_

//class MouseControlM
//{
//
//	bool isPresL, downL, isPresR, downR = false;
//	float mX, mY = 0;
//	
//public:
//	RenderWindow windowC;
//	MouseControlM(RenderWindow &window)
//	{
//		windowC = window;
//	}
//	//����� �����. ������� � ������� ��������� ������� (bugged)
//	void mouseCheck()
//	{
//
//		//if (!Mouse::isButtonPressed(Mouse::Left)) { isPresL = false; }
//		//if (!Mouse::isButtonPressed(Mouse::Right)) { isPresR = false; }
//		cout << "L " << isPresL << " R " << isPresR << endl; //������� ����� � �������
//	}
//	//��������� ���� ����� ������� ����. ���������� bool.
//	bool mouseClickLeft()
//	{
//		if (Mouse::isButtonPressed(Mouse::Left))
//		{
//			if (isPresL == false)
//			{
//				isPresL = true;
//				return true;
//			}
//			else return false;
//		}
//		else
//		{
//			isPresL = false;
//			return false;
//		}
//	}
//	//��������� ���� ������ ������� ����. ���������� bool.
//	bool mouseClickRight()
//	{
//		if (Mouse::isButtonPressed(Mouse::Right))
//		{
//			if (isPresR == false)
//			{
//				isPresR = true;
//				return true;
//			}
//			else return false;
//		}
//		else
//		{
//			isPresR = false;
//			return false;
//		}
//	}
//	//��������� ������ �� ����� ������ ����. ���������� bool.
//	bool mouseDownLeft()
//	{
//
//		if (Mouse::isButtonPressed(Mouse::Left))
//		{
//			isPresL = true;
//			return true;
//		}
//
//		else { isPresL = false; return false; }
//	}
//	//��������� ������ �� ������ ������ ����. ���������� bool.
//	bool mouseDownRight()
//	{
//
//		if (Mouse::isButtonPressed(Mouse::Right))
//		{
//			isPresR = true;
//			return true;
//		}
//		else { isPresR = false; return false; }
//	}
//	//��������� ����������� ����� ������ ����. ��������� ������ down. ���������� bool. 
//	bool mouseUpLeft()
//	{
//		if (downL == true)
//		{
//			if (!Mouse::isButtonPressed(Mouse::Left)) { isPresL = false; return true; }
//		}
//		else return false;
//	}
//	//��������� ����������� ������ ������ ����. �������� ������ down. ���������� bool.
//	bool mouseUpRight()
//	{
//		if (downL == true)
//		{
//			if (!Mouse::isButtonPressed(Mouse::Right)) { isPresR = false; return true; }
//		}
//		else return false;
//	}
//	//��������� ����������������� ��������� ����
//	float mousePosCords()
//	{
//
//		mX = Mouse::getPosition(windowC).x;
//
//		mY = Mouse::getPosition(windowC).y;
//
//	}
//
//
//	//���������� Vector2i ������� �������
//	Vector2i mousePos()
//	{
//
//		return Vector2i(Mouse::getPosition(windowC));
//
//	}
//
//
//};