#pragma once
#include "PEntity.h"


using namespace sf;
using namespace tmx;








class Player :public PEntity
{
public:
	Player(String Name, MapObject o, b2World& world) :PEntity( Name, o, world)
	{
		//obj = lev.GetAllObjects();
		me = BodyCreator::Add(o, world, b2_dynamicBody);
		
		sprite.setTexture(rm->getTexture("RunComboSprite.png"));
		sprite.setTextureRect(IntRect(0,0,33,36));

		sprite.setPosition(x, y);


		b2Fixture* f = me->GetFixtureList();
		b2Shape::Type shapeType = f->GetType();
		Vector2f pos = o.GetPosition();
		
		sprite.setScale(w / sprite.getGlobalBounds().width, h / sprite.getGlobalBounds().height);
		if (shapeType == b2Shape::e_polygon)
		{

			b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();
			int count = ps->GetVertexCount();
			int iter;





			if (count == 4)
			{
				vector<Vector2f> tester;
				for (iter = 0; iter < count; iter++)
				{
					cout << "isRect. x= " << BoxToSfVec(ps->GetVertex(iter)).x << " y = " << BoxToSfVec(ps->GetVertex(iter)).y << " h= " << h << " w = " << w << endl;
					tester.push_back(tmx::BoxToSfVec(ps->GetVertex(iter)));
				}

				if (isRectangle(tester)) sprite.setOrigin(w/2,h/2);

			}
		}



		//rotAngle = rotateAngle;
		//if(rotateAngle !=0)
		//sprite.setRotation(rotAngle);
	}
	void update(Time time)
	{
		debugBorder();

		move();

		if (w >= 33)
		{
			x = BoxToSfFloat(me->GetPosition().x) - w / 2;
			y = -BoxToSfFloat(me->GetPosition().y) - h / 2;
		}
		else
		{
			x = BoxToSfFloat(me->GetPosition().x);
			y = -BoxToSfFloat(me->GetPosition().y);
		}
		sprite.setRotation(tmx::BoxToSfAngle(me->GetAngle()));


		sprite.setPosition(/*BoxToSfFloat(me->GetPosition().x)*/x /*+ w / 2*/, /*-BoxToSfFloat(me->GetPosition().y)*/y /*+ h / 2*/);
	}

	void move()
	{
		b2Vec2 plVel = me->GetLinearVelocity();
		float angleVel = me->GetAngularVelocity();
		if (Keyboard::isKeyPressed(Keyboard::W))
		{
			if (plVel.y < 20)
			{
				me->ApplyForceToCenter(b2Vec2(0, 5), true);
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::D))
		{
			if (plVel.x < 20)
			{
				me->ApplyForceToCenter(b2Vec2(5, 0), true);
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::A))
		{
			if (plVel.x > -20)
			{
				me->ApplyForceToCenter(b2Vec2(-5, 0), true);
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::S))
		{
			if (plVel.y > -20)
			{
				me->ApplyForceToCenter(b2Vec2(0, -5), true);
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::LShift))
		{
			b2Vec2 vel = me->GetLinearVelocity();
			me->ApplyForceToCenter(b2Vec2(-vel.x * 5, -vel.y * 5), true);
		}
	}

};