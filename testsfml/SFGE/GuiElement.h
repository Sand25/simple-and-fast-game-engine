#ifndef GUIELEMENT_H
#define GUIELEMENT_H
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

namespace sfge
{
    /// \brief The base class for objects that can be added to a GuiSystem
    class GuiElement
    {
        public:
            /// \brief Default Constructor.
            GuiElement();
            virtual ~GuiElement();

            /// \brief Handle the events received from the sf::RenderWindow.
            virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) = 0;

            /// \brief Draw the element.
            virtual void draw(sf::RenderWindow& window) = 0;

            /// \brief Set the position.
            virtual void setPosition(int x, int y) = 0;

            /// \brief Set the position.
            void setPosition(sf::Vector2i pos);

        private:
    };

}

#endif // GUIELEMENT_H
