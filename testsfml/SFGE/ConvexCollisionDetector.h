#ifndef CONVEXCOLLISIONDETECTOR_H
#define CONVEXCOLLISIONDETECTOR_H

#include "ConvexCollidable.h"

namespace sfge
{
    /// \brief A class able to detect collisions between objects derived from ConvexCollidable
    class ConvexCollisionDetector
    {
        public:
            ConvexCollisionDetector();
            ~ConvexCollisionDetector();

            /// \brief Adds a new collidable to the object pool.
            void add(ConvexCollidable* obj);

            /// \brief Removes the object from the pool.
            void remove(ConvexCollidable* obj);

            /// \brief Detects all collisions between added objects. Should be called every frame/timestep.
            void detectCollisions();

            /// \brief Detect collisions with a specific object. The object does not need to be added.
            void detectCollisionsWithObject(ConvexCollidable* obj);

            void setMTVOffset(float offset);


        private:
            std::vector<ConvexCollidable*> staticObjects;
            std::vector<ConvexCollidable*> dynamicObjects;
            bool collide(ConvexShape &obj1, ConvexShape &obj2, sf::Vector2f& mtv, float& overlap);
            float mtvoffset;
    };

}
#endif // CONVEXCOLLISIONDETECTOR_H
