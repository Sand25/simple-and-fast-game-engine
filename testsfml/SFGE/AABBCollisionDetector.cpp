#include "AABBCollisionDetector.h"
#include "AABBCollidable.h"

namespace sfge
{
    AABBCollisionDetector::AABBCollisionDetector()
    {

    }

    AABBCollisionDetector::~AABBCollisionDetector()
    {
        //dtor
    }

    void AABBCollisionDetector::add(AABBCollidable* obj)
    {

        if(obj->isDynamic())
            dynamicObjects.push_back(obj);
        else
            staticObjects.push_back(obj);
    }

    void AABBCollisionDetector::remove(AABBCollidable* obj)
    {
        if(obj->isDynamic())
        {
            for(auto itr = dynamicObjects.begin(); itr != dynamicObjects.end(); itr++)
            {
                if((*itr) == obj)
                {
                    itr = dynamicObjects.erase(itr);
                    return;
                }
            }
        }

        else
        {
            for(auto itr = staticObjects.begin(); itr != staticObjects.end(); itr++)
            {
                if((*itr) == obj)
                {
                    itr = staticObjects.erase(itr);
                    return;
                }
            }
        }
    }

    void AABBCollisionDetector::detectCollisions()
    {
        //We only need to test moving bodies for collision
        for(int i = 0; i < dynamicObjects.size(); i++)
        {
            //Test with other moving bodies
            for(int j = i+1; j < dynamicObjects.size(); j++)
            {
                if(dynamicObjects[i]->getRect().intersects(dynamicObjects[j]->getRect()))
                {
                    dynamicObjects[i]->onCollision(dynamicObjects[j]);
                    dynamicObjects[j]->onCollision(dynamicObjects[i]);
                }
            }

            //Test with static bodies
            for(int k = 0; k < staticObjects.size(); k++)
            {
                if(dynamicObjects[i]->getRect().intersects(staticObjects[k]->getRect()))
                {
                    dynamicObjects[i]->onCollision(staticObjects[k]);
                }
            }
        }
    }

    void AABBCollisionDetector::detectCollisionsWithObject(AABBCollidable* obj)
    {
        //Test with other moving bodies
        for(int j = 0; j < dynamicObjects.size(); j++)
        {
            if(obj->getRect().intersects(dynamicObjects[j]->getRect()))
            {
                obj->onCollision(dynamicObjects[j]);
                dynamicObjects[j]->onCollision(obj);
            }
        }
        //Test with static bodies
        for(int k = 0; k < staticObjects.size(); k++)
        {
            if(obj->getRect().intersects(staticObjects[k]->getRect()))
            {
                obj->onCollision(staticObjects[k]);
                staticObjects[k]->onCollision(obj);
            }
        }
    }
}
