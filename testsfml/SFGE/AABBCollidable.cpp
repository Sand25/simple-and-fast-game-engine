#include "AABBCollidable.h"

namespace sfge
{
    AABBCollidable::AABBCollidable(const std::string& name, bool dynamic) : name(name), dynamic(dynamic)
    {
        //ctor
    }

    AABBCollidable::~AABBCollidable()
    {

    }

    std::string AABBCollidable::getName()
    {
        return name;
    }

    bool AABBCollidable::isDynamic()
    {
        return dynamic;
    }
}
