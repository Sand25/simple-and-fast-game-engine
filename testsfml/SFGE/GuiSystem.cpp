#include "GuiSystem.h"

namespace sfge
{
    GuiSystem::GuiSystem(sf::RenderWindow& window) : window(window)
    {
        //ctor
    }

    GuiSystem::~GuiSystem()
    {
        for(GuiElement* element : elements)
            delete element;
        elements.clear();
    }

    void GuiSystem::addElement(GuiElement* element)
    {
        elements.push_back(element);
    }

    void GuiSystem::handleEvent(const sf::Event& event)
    {
        for(GuiElement* element : elements)
            element->handleEvent(event, window);
    }

    void GuiSystem::draw()
    {
        for(GuiElement* element : elements)
            element->draw(window);
    }

}
