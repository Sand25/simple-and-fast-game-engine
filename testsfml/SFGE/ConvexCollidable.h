#ifndef CONVEXCOLLIDABLE_H
#define CONVEXCOLLIDABLE_H
#include "ConvexShape.h"

namespace sfge
{
    /// \brief The base class for objects that can be added to a ConvexCollisionDetector.
    class ConvexCollidable
    {
        public:
            /// \brief Constructor.
            /// @param name This string is used to distinguish different types of objects.\n
            ///        It's convenient to check what type of object you've collided with.
            /// @param dynamic Wether or not the body moves.
            ConvexCollidable(std::string name, bool dynamic = true);
            ~ConvexCollidable();

            /// \brief Returns the shape.
            ConvexShape& getShape();

            /// \brief Returns the name of the object.
            std::string getName();

            /// \brief Returns true if the object is dynamic, otherwise false.
            bool isDynamic();

            /// \brief This function is called when a collision occurs.
            /// \param obj The object we've collided with.
            /// \param mtv The Minimum Translation Vector (the shortest way out of the other object).
			virtual void onCollision(ConvexCollidable* obj, sf::Vector2f mtv) = 0;

        protected:
            ConvexShape shape;

        private:
            std::string name;
            bool dynamic;
    };

}
#endif // CONVEXCOLLIDABLE_H
