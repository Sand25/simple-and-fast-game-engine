#include "ConvexCollidable.h"

namespace sfge
{

    ConvexCollidable::ConvexCollidable(std::string name, bool dynamic) : name(name), dynamic(dynamic)
    {
        //ctor
    }

    ConvexCollidable::~ConvexCollidable()
    {
        //dtor
    }

    ConvexShape& ConvexCollidable::getShape()
    {
        return shape;
    }

    std::string ConvexCollidable::getName()
    {
        return name;
    }

    bool ConvexCollidable::isDynamic()
    {
        return dynamic;
    }

}
