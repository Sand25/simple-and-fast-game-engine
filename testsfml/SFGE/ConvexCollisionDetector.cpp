#include "ConvexCollisionDetector.h"
#include <iostream>

namespace sfge
{


	double dotp(const sf::Vector2f& v1, const sf::Vector2f& v2)
	{
		return v1.x * v2.x + v1.y * v2.y;
	}

	sf::Vector2f perp(const sf::Vector2f& vec)
	{
		return sf::Vector2f(-vec.y, vec.x);
	}

    ConvexCollisionDetector::ConvexCollisionDetector() : mtvoffset(0)
    {
        //ctor
    }

    ConvexCollisionDetector::~ConvexCollisionDetector()
    {
        //dtor
    }

    void ConvexCollisionDetector::add(ConvexCollidable* obj)
    {

        if(obj->isDynamic())
            dynamicObjects.push_back(obj);
        else
            staticObjects.push_back(obj);
    }

    void ConvexCollisionDetector::remove(ConvexCollidable* obj)
    {
        if(obj->isDynamic())
        {
            for(auto itr = dynamicObjects.begin(); itr != dynamicObjects.end(); itr++)
            {
                if((*itr) == obj)
                {
                    itr = dynamicObjects.erase(itr);
                    return;
                }
            }
        }

        else
        {
            for(auto itr = staticObjects.begin(); itr != staticObjects.end(); itr++)
            {
                if((*itr) == obj)
                {
                    itr = staticObjects.erase(itr);
                    return;
                }
            }
        }
    }

    void ConvexCollisionDetector::detectCollisions()
    {
		extern bool debugMode;
        float overlap;
        sf::Vector2f mtv;
        //We only need to test moving bodies for collision
        for(int i = 0; i < dynamicObjects.size(); i++)
        {
            //Test with other moving bodies
            for(int j = i+1; j < dynamicObjects.size(); j++)
            {
                if(collide(dynamicObjects[i]->getShape(), dynamicObjects[j]->getShape(), mtv, overlap))
                {
					if (debugMode==true) std::cout << "Collide this dinamic: " + dynamicObjects[i]->getName() + " Whith "+ dynamicObjects[j]->getName() << std::endl;
                    dynamicObjects[i]->onCollision(dynamicObjects[j], mtv*overlap);
                    dynamicObjects[j]->onCollision(dynamicObjects[i], -mtv*overlap);
                }
            }

            //Test with static bodies
            for(int k = 0; k < staticObjects.size(); k++)
            {
                if(collide(dynamicObjects[i]->getShape(), staticObjects[k]->getShape(), mtv, overlap))
                {
					if (debugMode == true) std::cout << "Collide this static: " + dynamicObjects[i]->getName() + " Whith " + staticObjects[k]->getName() << std::endl;
                    dynamicObjects[i]->onCollision(staticObjects[k], mtv*overlap);
                    staticObjects[k]->onCollision(dynamicObjects[i], -mtv*overlap);
                }
            }
        }
    }

    void ConvexCollisionDetector::detectCollisionsWithObject(ConvexCollidable* obj)
    {
        float overlap;
        sf::Vector2f mtv;
        //Test with other moving bodies
        for(int j = 0; j < dynamicObjects.size(); j++)
        {
            if(collide(obj->getShape(), dynamicObjects[j]->getShape(), mtv, overlap))
            {
                obj->onCollision(dynamicObjects[j], mtv*overlap);
                dynamicObjects[j]->onCollision(obj, -mtv*overlap);
            }
        }
        //Test with static bodies
        for(int k = 0; k < staticObjects.size(); k++)
        {
            if(collide(obj->getShape(), staticObjects[k]->getShape(), mtv, overlap))
            {
                obj->onCollision(staticObjects[k], mtv*overlap);
                staticObjects[k]->onCollision(obj, -mtv*overlap);
            }
        }
    }

    bool ConvexCollisionDetector::collide(ConvexShape &obj1, ConvexShape &obj2, sf::Vector2f& mtv, float& overlap)
    {
        overlap = 1000;
        std::vector<sf::Vector2f>& object1 = obj1.getPoints();
        std::vector<sf::Vector2f>& object2 = obj2.getPoints();
        int nume = object1.size();
        for(int i=0; i<nume; i++)
        {
            sf::Vector2f edge = object1[(i+1)%nume] - object1[i];
            sf::Vector2f normal = perp(edge);

            float len = sqrt(normal.x*normal.x + normal.y*normal.y);
            normal.x /= len;
            normal.y /= len;

            double min1 = std::numeric_limits<double>::infinity();
            double min2 = min1;
            double max1 = -std::numeric_limits<double>::infinity();
            double max2 = max1;

            for(int j=0; j<object1.size(); j++)
            {
                double dot = dotp(normal, object1[j]);
                min1 = std::min(min1, dot);
                max1 = std::max(max1, dot);
            }
            for(int j=0; j<object2.size(); j++)
            {
                double dot = dotp(normal, object2[j]);
                min2 = std::min(min2, dot);
                max2 = std::max(max2, dot);
            }

            if(min2 > max1 || min1 > max2)
                return false;
            else
            {
                float x3 = std::max(min1, min2);
                float y3 = std::min(max1, max2);
                float length = std::max(y3, x3) - std::min(x3, y3);
                if(length < overlap)
                {
                    mtv.x = normal.x;
                    mtv.y = normal.y;
                    sf::Vector2f d;
                    d.x = obj1.getCenter().x - obj2.getCenter().x;
                    d.y = obj1.getCenter().y - obj2.getCenter().y;
                    if (dotp(normal, d) < 0)
                    {
                        mtv = -mtv;
                    }
                    overlap = length;
                }
            }
        }

        nume = object2.size();
        for(int i=0; i<nume; i++)
        {
            sf::Vector2f edge = object2[(i+1)%nume] - object2[i];
            sf::Vector2f normal = perp(edge);

            float len = sqrt(normal.x*normal.x + normal.y*normal.y);
            normal.x /= len;
            normal.y /= len;

            double min1 = std::numeric_limits<double>::infinity();
            double min2 = min1;
            double max1 = -std::numeric_limits<double>::infinity();
            double max2 = max1;

            for(int j=0; j<object2.size(); j++)
            {
                double dot = dotp(normal, object2[j]);
                min1 = std::min(min1, dot);
                max1 = std::max(max1, dot);
            }
            for(int j=0; j<object1.size(); j++)
            {
                double dot = dotp(normal, object1[j]);
                min2 = std::min(min2, dot);
                max2 = std::max(max2, dot);
            }

            if(min2 > max1 || min1 > max2)
                return false;
            else
            {
                float x3 = std::max(min1, min2);
                float y3 = std::min(max1, max2);
                float length = std::max(y3, x3) - std::min(x3, y3);
                if(length < overlap)
                {
                    mtv.x = normal.x;
                    mtv.y = normal.y;
                    sf::Vector2f d;
                    d.x = obj2.getCenter().x - obj1.getCenter().x;
                    d.y = obj2.getCenter().y - obj1.getCenter().y;
                    if (dotp(normal, d) > 0)
                    {
                        mtv = -mtv;
                    }
                    overlap = length;
                }
            }
        }

        overlap += mtvoffset;
        return true;

    }

    void ConvexCollisionDetector::setMTVOffset(float offset)
    {
        mtvoffset = offset;
    }

}
