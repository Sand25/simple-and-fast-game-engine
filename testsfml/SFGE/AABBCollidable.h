#ifndef COLLIDABLE_H
#define COLLIDABLE_H
#include <string>
#include <SFML/Graphics.hpp>

namespace sfge
{

    /// \brief The base class for objects that can be added to an AABBCollisionDetector.
    class AABBCollidable
    {

        public:
            /// \brief Constructor.
            /// @param name This string is used to distinguish different types of objects.\n
            ///        It's convenient to check what type of object you've collided with.
            /// @param dynamic Wether or not the body moves.
            AABBCollidable(const std::string& name, bool dynamic = true);
            virtual ~AABBCollidable();

            /// \brief Returns the bounding-box of the object. Needs to be overrided.
            virtual sf::FloatRect getRect() = 0;

            /// \brief Gets called for every object it collides with.
            virtual void onCollision(AABBCollidable* obj){};

            /// \brief Returns the name of the object.
            std::string getName();

            /// \brief Returns true if it is a moving body.
            bool isDynamic();

        protected:

        private:
            std::string name;
            bool dynamic;
    };
}

#endif // COLLIDABLE_H
