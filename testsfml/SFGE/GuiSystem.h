#ifndef GUISYSTEM_H
#define GUISYSTEM_H
#include "GuiElement.h"

namespace sfge
{
    /// \brief Handles the menu in a game.
    class GuiSystem
    {
        public:
            /// \brief Constructor.
            GuiSystem(sf::RenderWindow& window);
            ~GuiSystem();

            /// \brief Add an element to the system.
            void addElement(GuiElement* element);

            /// \brief Handles the event for all the elements.
            void handleEvent(const sf::Event& event);

            /// \brief Draws all the elements
            void draw();

        private:
            sf::RenderWindow& window;
            std::vector<GuiElement*> elements;
    };

}

#endif // GUISYSTEM_H
