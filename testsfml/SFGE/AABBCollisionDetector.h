#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H
#include <cmath>
#include <vector>

namespace sfge
{
    class AABBCollidable;

    /// \brief A class able to detect collisions between objects derived from AABBCollidable.
    ///
    /// Please be aware this is a brute-force implementation. If you need very much collision detection this class is not suited.\n
    /// Performance-wise it is O(n^2).
    class AABBCollisionDetector
    {
        public:
            /// \brief Default contructor.
            AABBCollisionDetector();
            ~AABBCollisionDetector();

            /// \brief Adds a new collidable to the object pool.
            void add(AABBCollidable* obj);

            /// \brief Removes the object from the pool.
            void remove(AABBCollidable* obj);

            /// \brief Detects all collisions between added objects. Should be called every frame/timestep.
            void detectCollisions();

            /// \brief Detect collisions with a specific object. The object does not need to be added.
            void detectCollisionsWithObject(AABBCollidable* obj);

        private:

            std::vector<AABBCollidable*> dynamicObjects;
            std::vector<AABBCollidable*> staticObjects;
    };
}

#endif // COLLISIONDETECTOR_H
