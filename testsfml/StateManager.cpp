#include "StateManager.h"



StateManager::StateManager(RenderWindow& window):window(window)
{
	mContrM.init();
}


StateManager::~StateManager()
{
	LOG("Deleting manager", Logger::Type::Info);
	for (auto it = gameStates.begin(); it != gameStates.end(); it++)
		delete (*it);
	gameStates.clear();
}

void StateManager::run(GameState * gameState)
{
	pushState(gameState);
	running = true;

	sf::Event event;

	clock.restart();

	while (running && window.isOpen())
	{
		if (!shouldPop)
		{
			//handle
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					window.close();
			}
			//update
			//accumulator += frameClock.getElapsedTime();
			mContrM.mousePosCords(window);
			elapsed = clock.getElapsedTime();
			if (elapsed.asSeconds() > 1/60.f )
			{		

			//mContrM.updateCursor();
			clock.restart();
			update();

			//draw
			window.clear();
			StateManager *sm = this;
			window.draw(*sm);
			window.display();

			}

		}
		else
		{
			doPop();
			clock.restart();
		}
	}
}

void StateManager::popState()
{
	shouldPop = true;
}

void StateManager::replaceState(GameState * gameState)
{
	LOG("Replacing gamestate", Logger::Type::Info);
	LOG("States: " + gameStates.size(), Logger::Type::Info);
	popState();
	// store and init the new state
	gameState->setManager(this);
	gameState->setWindow(&window);
	gameStates.insert(gameStates.end() - 1, gameState);
	LOG("States in query: " + gameStates.size(), Logger::Type::Info);
}

void StateManager::pushState(GameState * gameState)
{
	if (gameState != NULL)
	{
	LOG("Pushing state", Logger::Type::Info);
	LOG("States: " + gameStates.size(), Logger::Type::Info);
	gameState->setManager(this);
	gameState->setWindow(&window);

	// pause current state
	if (!gameStates.empty())
		gameStates.back()->onPause();

	// store and init the new state
	gameStates.push_back(gameState);
	LOG("States in query: " + gameStates.size(), Logger::Type::Info);
	}

}

void StateManager::gameClose()
{
	gameStates.resize(0);
	shouldPop = true;
}

void StateManager::clearQuery(GameState* state)
{
	//gameStates.resize(gameStates.size());
	//for (int i = 0; i < gameStates.size(); i++)
	//{
	//	gameStates.erase(gameStates.begin() + i);
	//}
	//for (int i = 0; i < gameStates.size(); i++)
	//{
	//	gameStates[i]->~GameState();
	//}
	//while (gameStates.size() != 0)
	//{
	//	gameStates.resize(gameStates.size());

	//	if (gameStates.size() > 0)
	//	{	
	//	gameStates.erase(gameStates.begin());
	//	}
	//	else 
	//	{
	//		gameStates.clear();
	//		break;
	//	}
	//		
	//}gameStates.clear();
	for (auto it = gameStates.begin(); it != gameStates.end(); it++)
		delete [] (*it);
	gameStates.clear();
	pushState(state);
}

String StateManager::getName()
{
	return name;
}

void StateManager::doPop()
{
	LOG("Popping state", Logger::Type::Info);
	LOG("States in query: " + gameStates.size(), Logger::Type::Info);
	shouldPop = false;
	if (!gameStates.empty())
	{
		LOG("Deleting curret gamestate", Logger::Type::Info);
		delete gameStates.back();
		LOG("Current gamestate deleted", Logger::Type::Info);
		gameStates.pop_back();
		if (!gameStates.empty())
			gameStates.back()->onResume();
		else
		{
			running = false;
			window.close();
		}
		LOG("States in querry left: " + gameStates.size(), Logger::Type::Info);
	}
}

void StateManager::update()
{
	if (!gameStates.empty())
		gameStates.back()->onUpdate();
}

void StateManager::draw(sf::RenderTarget & window, sf::RenderStates states) const
{
	if (!gameStates.empty())
	{
		for (auto& it = gameStates.begin(); it != gameStates.end() - 1; it++)
		{
			GameState *b = *it;
			window.draw(*b);
		}
		GameState *b = gameStates.back();
		window.draw(*b);

		window.draw(mContrM.pointer);
	}

}
