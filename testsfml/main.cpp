#include <SFML\Graphics.hpp>
#include "view.h"
#include <iostream>
#include <sstream>
//#include "mission.h"
#include "iostream"
#include "level.h"
#include <vector>
#include <list>
//#include "menu.h"
//#include "AnimationManager.h"
#include "ResourceManager.h"
#include "global.h"
#include "ParticleSystem.h"
#include "VectorControl.h"
#include <algorithm>
#include <Box2D\Box2D.h>
#include "MouseControl.h"

#include <tmx\tmx2box2d.h>
#include <tmx\MapLoader.h>
//#include "MapL.h"
#include <tmx\DebugShape.h>
#define LOG_OUTPUT_ALL
#include <memory>
//#include "SFGE\Director.h"
//#include "SFGE\State.h"
#include <thread>
#include <tmx\Log.h>
#include "GameState.h"
#include "StateManager.h" 
#include "GameStates.h"


Sound buttonPress;


b2Vec2 martixRot(b2Vec2 vector, float angle)
{
	float matrix[4];
	matrix[0] = cos(angle);
	matrix[1] = -sin(angle);
	matrix[2] = sin(angle);
	matrix[3] = cos(angle);
	vector.x = vector.x * matrix[0]; vector.y = vector.y * matrix[2];
	vector.x = vector.x * matrix[1]; vector.y = vector.y * matrix[3];
	return vector;
}



 
bool debugMode = false;
//bool isMenu = true;
//int menuNum;

Font txt;
bool reLoadLvl = true;
Text text;

using namespace sf;
using namespace tmx;

Vector2f curMousePos = Vector2f(0,0);


//bool isRectangle(vector<Vector2f> tester)
//{
//	int iter;
//
//
//	if (tester.size() != 4)
//	{
//		std::cout << "Error: unexpected loop in entity class loader" << std::endl;
//	}
//	else
//	{
//		float x1, y1, x2, y2, x3, y3, x4, y4;
//		x1 = tester[0].x; y1 = tester[0].y;
//		x2 = tester[1].x; y2 = tester[1].y;
//		x3 = tester[2].x; y3 = tester[2].x;
//		x4 = tester[3].x; y4 = tester[3].x;
//		cout << "X =" << x1 << " " << x2 << " " << x3 << " " << x4 << endl;
//		cout << "y =" << y1 << " " << y2 << " " << y3 << " " << y4 << endl;
//		if (x1 == x4 && x2 == x3 )
//		{
//			if (y1 == y2 && y3 == -y4)
//			{
//				cout << "sucses" << endl;
//				return true;
//			}
//		}
//	}
//	cout << "failure" << endl;
//	return false;
//}




class RectDrw :public Drawable, public Transformable
{
public:
	RectangleShape rectangleD;
	float angle = 0;
	RectDrw(float X,float Y,float W,float H, float Angle)
	{
		rectangleD.setFillColor(Color::Green);
		rectangleD.setPosition(Vector2f(X,Y));
		rectangleD.setSize(Vector2f(W,H));
		rectangleD.setRotation(Angle);
	}

private:

	std::vector<sf::Vector2f> points;
	sf::Vector2f pos;
	sf::VertexArray vertices;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		// apply the transform
		states.transform *= getTransform();

		// our particles don't use a texture
		//states.texture = NULL;

		// draw the vertex array
		target.draw(rectangleD, states);
	}

};

//----------------------------------------------------------box2D------------------------------------------------------------------------------------------------

//////////////////////////////////////////////////////////box2D////////////////////////////////////////////////////////////////////////////////////////////////////////


class pointS 
{
public:
	Texture pointT;
	Sprite sprite;
	float x, y, offsetx2,offsety2;
	float w = 1;
	float h = 1;
	pointS() 
	{
		pointT.loadFromFile("Resources/point.png");
		sprite.setTexture(pointT);
	}
	void update(float X, float Y, float offsetx, float offsety) 
	{
		x = X; y = Y; offsetx2 = offsetx; offsety2 = offsety;
		sprite.setPosition(x+offsetx,y+offsety);

	}
	FloatRect getRect() 
	{
		return FloatRect(x+ offsetx2, y+ offsety2, w, h);
	}
};


//class PEntity :public Drawable, public Transformable {
//protected:
//
//public:
//
//	float dx, dy, x, y, rotAngle, speed, moveTimer, tdx, tdy, tx, ty, velocity;
//	int w, h, health;
//	bool life, isMove, onGround, canMove;
//	Texture texture;
//	Sprite sprite;
//	String name;
//	Vector2f curPos, targetPos, posTmp, velocityV, directionV;
//
//	b2Body* me;
//	b2World& world;
//
//	Vertex vert;
//
//	CircleShape circle;
//	
//
//	std::vector<std::unique_ptr<sf::Shape>> debugBoxes;
//	std::vector<DebugShape> debugShapes;
//
//	PEntity(Image &image, String Name, tmx::MapObject o, b2World &world) :world(world)
//	{
//		x = o.GetPosition().x; y = o.GetPosition().y; w = o.GetAABB().height; h = o.GetAABB().width; name = Name; moveTimer = 0;
//		speed = 0; health = 100; dx = 0; dy = 0;
//		life = true; onGround = false; isMove = false;
//		texture.loadFromImage(image);
//		sprite.setTexture(texture);
//		
//
//
//
//
//		
//
//		if (o.GetShapeType() == Circle)
//		{
//			if(w <= 32)
//			{
//			sprite.setOrigin(w / 2, h / 2);
//			}
//
//		}
//
//		//me = BodyCreator::Add(o, world);
//		//debugBorder();
//	}
//
//	~PEntity()
//	{
//		me->DestroyFixture(me->GetFixtureList());
//		points.clear();
//	}
//
//	FloatRect getRect()
//	{
//		return FloatRect(x, y, w, h);
//	}
//
//
//
//	virtual void update(Time time) = 0;
//
//
//	void debugBorder()
//	{
//
//		if (!debugBoxes.empty())
//		{
//			for (int i = 0; i < debugBoxes.size(); i++)
//			{
//				debugBoxes.erase(debugBoxes.begin() + i);
//
//			}
//			debugBoxes.clear();
//		}
//		if (!debugShapes.empty())
//		{
//			//for (auto& itr = debugShapes.begin(); itr != debugShapes.end(); itr++)
//			//{
//			//
//			//debugShapes.erase(itr);
//
//			//}
//			debugShapes.clear();
//		}
//
//		debugBoxes.push_back(std::unique_ptr<sf::RectangleShape>(new sf::RectangleShape(sf::Vector2f(6.f, 6.f))));
//		sf::Vector2f pos = tmx::BoxToSfVec(me->GetPosition());
//		debugBoxes.back()->setPosition(pos);
//		debugBoxes.back()->setOrigin(3.f, 3.f);
//
//		for (b2Fixture* f = me->GetFixtureList(); f; f = f->GetNext())
//		{
//			b2Shape::Type shapeType = f->GetType();
//			if (shapeType == b2Shape::e_polygon)
//			{
//				DebugShape ds;
//				ds.setPosition(pos);
//				b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();
//
//				int count = ps->GetVertexCount();
//				for (int i = 0; i < count; i++)
//					ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(i)), sf::Color::Green));
//
//				ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0)), sf::Color::Green));
//				debugShapes.push_back(ds);
//			}
//			else if (shapeType == b2Shape::e_circle)
//			{
//				b2CircleShape* cs = static_cast<b2CircleShape*>(f->GetShape());
//				float radius = tmx::BoxToSfFloat(cs->m_radius);
//				std::unique_ptr<sf::CircleShape> c(new sf::CircleShape(radius));
//				c->setPosition(pos);
//				c->setOrigin(radius, radius);
//				c->setOutlineColor(sf::Color::Green);
//				c->setOutlineThickness(-1.f);
//				c->setFillColor(sf::Color::Transparent);
//				debugBoxes.push_back(std::move(c));
//			}
//		}
//	}
//
//
//private:
//
//	std::vector<sf::Vector2f> points;
//	sf::Vector2f pos;
//	std::vector<sf::Vertex> vertices;
//
//	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
//	{
//		// apply the transform
//		states.transform *= getTransform();
//		states.texture = &texture;
//		
//		// our particles don't use a texture
//		//states.texture = NULL;
//
//		// draw the vertex array
//		target.draw(sprite, states);
//		
//
//	}
//
//
//};


class Entity :public Drawable, public Transformable {
protected:
	
public:

	float dx, dy, x, y,rotAngle, speed,moveTimer, tdx, tdy, tx, ty, velocity;
	int w,h,health;
	bool life, isMove, onGround, canMove;
	Texture texture;
	Sprite sprite;
	String name;
	Vector2f curPos, targetPos,posTmp,velocityV,directionV;


	

	Entity(Image &image, String Name, float X, float Y, int W, int H) 
	{
		x = X; y = Y; w = W; h = H; name = Name; moveTimer = 0;
		speed = 0; health = 100; dx = 0; dy = 0;
		life = true; onGround = false; isMove = false;
		texture.loadFromImage(image);
		sprite.setTexture(texture);
		//sprite.setOrigin(w / 32, h / 32);
	}

	~Entity()
	{
		points.clear();
	}

	FloatRect getRect()
	{
		return FloatRect(x, y, w, h);
	}



	virtual void update(Time time) = 0;
	




private:

	std::vector<sf::Vector2f> points;
	sf::Vector2f pos;
	std::vector<sf::Vertex> vertices;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		// apply the transform
		states.transform *= getTransform();

		// our particles don't use a texture
		//states.texture = NULL;

		// draw the vertex array
		target.draw(sprite, states);
	}


};



//class Slider :public PEntity
//{
//public:
//	Slider(Image &image, String Name, MapObject o, b2World& world) :PEntity(image, Name, o, world)
//	{
//		//obj = lev.GetAllObjects();
//
//		me = BodyCreator::Add(o, world);
//
//
//		b2Fixture* f = me->GetFixtureList();
//		b2Shape::Type shapeType = f->GetType();
//		Vector2f pos = o.GetPosition();
//
//		
//		if (shapeType == b2Shape::e_polygon)
//		{
//
//			b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();
//			int count = ps->GetVertexCount();
//			int iter;
//
//
//
//
//
//			if (count == 4)
//			{
//				vector<Vector2f> tester;
//				for (iter = 0; iter < count; iter++)
//				{
//					cout << "isRect. x= " << BoxToSfVec(ps->GetVertex(iter)).x << " y = " << BoxToSfVec(ps->GetVertex(iter)).y << " h= " << h << " w = " << w << endl;
//					tester.push_back(tmx::BoxToSfVec(ps->GetVertex(iter)));
//				}
//
//				isRect = isRectangle(tester);
//
//			}
//			if (!isRect)
//			{
//				for (iter = 0; iter < count; iter++)
//					vertices.push_back(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(iter))));
//
//				vertices.push_back(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0))));
//				/*vertices[iter+1].position = pos;*/
//				for (iter = 0; iter < vertices.size(); iter++)
//				{
//					vertices[iter].position = Vector2f(vertices[iter].position.x + pos.x, vertices[iter].position.y + pos.y);
//				}
//			}
//
//		}
//		if (o.GetName() == "Circle"|| o.GetName() == "circle")
//		{
//			cout << "Circle" << endl;
//			float radius = h / 2;
//			circle.setRadius(radius);
//			circle.setPosition(pos);
//			//circle.setOrigin(radius, radius);
//			circle.setOutlineColor(sf::Color::Green);
//			circle.setOutlineThickness(-1.f);
//			circle.setTexture(new Texture(texture));
//		}
//		if (isRect == true)
//		{
//			//cout << "isRect. x= " <<x <<" y = "<< y <<" h= "<<h <<" w = "<<w << endl;
//			sprite.setOrigin(w/2,h/2);
//			
//		}
//
//		x = BoxToSfFloat(me->GetPosition().x) + w / 2 - 16;
//		y = -BoxToSfFloat(me->GetPosition().y) + h / 2 - 16;
//
//		
//
//		//sprite.setPosition(x,y);
//		sprite.setPosition(x /*+ w / 2*/, y /*+ h / 2*/);
//
//		sprite.setScale(h/32,w/32);
//		//rotAngle = rotateAngle;
//		//if(rotateAngle !=0)
//		//sprite.setRotation(rotAngle);
//	}
//	void update(Time time)
//	{
//		x = BoxToSfFloat(me->GetPosition().x) + w/2 - 16;
//		y = -BoxToSfFloat(me->GetPosition().y) + h/2 -16;
//		
//		debugBorder();
//
//		//sprite.setPosition(x,y);
//		sprite.setPosition(x /*+ w / 2*/, y /*+ h / 2*/);
//	}
//private:
//
//	bool isRect = false;
//	std::vector<sf::Vector2f> points;
//	sf::Vector2f pos;
//	std::vector<sf::Vertex> vertices;
//
//	void draw(sf::RenderTarget& target, sf::RenderStates states) const
//	{
//		states.transform *= getTransform();
//		states.texture = &texture;
//		if (!isRect)
//		{
//		target.draw(&vertices[0], vertices.size(), TrianglesStrip, states);
//		}
//		else target.draw(sprite);
//		
//		
//		target.draw(circle);
//	}
//};
//
//
//class Player :public PEntity
//{
//public:
//	Player(Image &image, String Name, MapObject o, b2World& world) :PEntity(image, Name, o, world)
//	{
//		//obj = lev.GetAllObjects();
//		me = BodyCreator::Add(o, world, b2_dynamicBody);
//
//		sprite.setPosition(x, y);
//
//		sprite.setScale(w/30, h/30);
//		//rotAngle = rotateAngle;
//		//if(rotateAngle !=0)
//		//sprite.setRotation(rotAngle);
//	}
//	void update(Time time)
//	{
//		debugBorder();
//
//		move();
//
//		if (w >= 33)
//		{
//			x = BoxToSfFloat(me->GetPosition().x) - w/2;
//			y = -BoxToSfFloat(me->GetPosition().y) - h/2;
//		}
//		else
//		{
//			x = BoxToSfFloat(me->GetPosition().x);
//			y = -BoxToSfFloat(me->GetPosition().y);
//		}
//
//
//
//		sprite.setPosition(/*BoxToSfFloat(me->GetPosition().x)*/x /*+ w / 2*/, /*-BoxToSfFloat(me->GetPosition().y)*/y /*+ h / 2*/);
//	}
//
//	void move()
//	{
//		b2Vec2 plVel = me->GetLinearVelocity();
//		float angleVel = me->GetAngularVelocity();
//		if (Keyboard::isKeyPressed(Keyboard::W))
//		{
//			if (plVel.y < 20)
//			{
//				me->ApplyForceToCenter(b2Vec2(0, 5), true);
//			}
//		}
//		if (Keyboard::isKeyPressed(Keyboard::D))
//		{
//			if (plVel.x < 20)
//			{
//				me->ApplyForceToCenter(b2Vec2(5, 0), true);
//			}
//		}
//		if (Keyboard::isKeyPressed(Keyboard::A))
//		{
//			if (plVel.x > -20)
//			{
//				me->ApplyForceToCenter(b2Vec2(-5, 0), true);
//			}
//		}
//		if (Keyboard::isKeyPressed(Keyboard::S))
//		{
//			if (plVel.y > -20)
//			{
//				me->ApplyForceToCenter(b2Vec2(0, -5), true);
//			}
//		}
//		if (Keyboard::isKeyPressed(Keyboard::LShift))
//		{
//			b2Vec2 vel = me->GetLinearVelocity();
//			me->ApplyForceToCenter(b2Vec2(-vel.x * 5, -vel.y * 5), true);
//		}
//	}
//
//};







bool menuState(RenderWindow& window)
{
	cout << "Thread Enter" << endl;
	Texture menuStartT, menuControlT, menuControlTS, menuExitT, menuExitTS, aboutTexture, solidMenuT, menuStartTS, menuExitTScontrolTextureT;
	isMenu = true; cout << "MenuMode enabled" << endl;

	int selector;
	
	menuStartT.loadFromFile("Resources/images/menu/SolidBackToGameNS.png");
	menuControlT.loadFromFile("Resources/images/menu/ControlsNS.png");
	menuExitT.loadFromFile("Resources/images/menu/SolidExitNS.png");

	aboutTexture.loadFromFile("Resources/images/menu/SolidBackToGameNS.png");

	menuStartTS.loadFromFile("Resources/images/menu/SolidBackToGameSel.png");
	menuExitTS.loadFromFile("Resources/images/menu/SolidExitSel.png");
	menuControlTS.loadFromFile("Resources/images/menu/ControlsSel.png");

	solidMenuT.loadFromFile("Resources/images/menu/SolidMenuEmpty.png");


	Sprite menuStart(menuStartT), menuControl(menuControlT), menuExit(menuExitT), about(aboutTexture), solidMenu(solidMenuT);

	menuStart.setPosition(200+offset.x, 150+offset.y);
	menuControl.setPosition(250 + offset.x, 200 + offset.y);
	menuExit.setPosition(230 + offset.x, 250 + offset.y);
	solidMenu.setPosition(70 + offset.x, 0 + offset.y);

	bool mouseOver = false;

	MouseControl mContr;
	mContr.init();

	while (isMenu == true && window.isOpen() == true)
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		mouseOver = false;

		mContr.mousePosCords(window);

		menuStart.setPosition(offset.x - 250/*200*/  , offset.y - 300/*150*/);
		menuControl.setPosition(offset.x - 250/*250*/ , offset.y - 250/*200*/);
		menuExit.setPosition(offset.x - 250, offset.y - 200);
		solidMenu.setPosition(offset.x - 400, offset.y - 400);

		menuStart.setTexture(menuStartT);
		menuExit.setTexture(menuExitT);
		//menuStart.setColor(Color::White);
		//menuControl.setColor(Color::White);
		//menuExit.setColor(Color::White);



		window.clear();




		if (menuNum == 1)
		{
			if (menuStart.getGlobalBounds().intersects(mContr.cursor.getGlobalBounds())) { /*menuStart.setColor(Color::Blue);*/ selector = 1; /*gameRunning();*/ mouseOver = true; menuStart.setTexture(menuStartTS); }
			if (menuControl.getGlobalBounds().intersects(mContr.cursor.getGlobalBounds())) { /*menu2.setColor(Color::Blue); menuStart.setTexture(menuStartT); menuExit.setTexture(menuExitT);*/ selector = 2; mouseOver = true; }
			if (menuExit.getGlobalBounds().intersects(mContr.cursor.getGlobalBounds())) {/* menuExit.setColor(Color::Blue);*/ selector = 3; mouseOver = true;  menuExit.setTexture(menuExitTS); }
		}


		window.draw(solidMenu);
		window.draw(menuStart);
		window.draw(menuExit);

		window.draw(mContr.pointer);

		if (mContr.mouseClickLeft() && mouseOver == true)
		{
			if (selector == 1) {  isMenu = false; reLoadLvl == true; return true; }//���� ������ ������ ������, �� ������� �� ���� 
			if (selector == 3) {  window.close(); isMenu = false; cout << "MenuMode disabled" << endl; return false; }
		}

		window.display();

	}
	
	cout << "Thread Exit" << endl;
	return true;

}










 
//class GameState :public Drawable
//{
//
//	friend class StateManager;
//
//public:
//
//	
//	GameState(RenderWindow* window, StateManager* manager);
//	~GameState();
//	
//	virtual void onUpdate();
//
//	
//
//	virtual void onPause();
//
//	virtual void onResume();
//
//protected:
//
//	StateManager* manager;
//	void setManager(StateManager* manager);
//
//
//	RenderWindow* window;
//	void setWindow(RenderWindow* window);
//
//
//private:
//	virtual void draw(RenderTarget& window, RenderStates state) const;
//
//
//};



//class StateManager :public Drawable
//{
//public:
//	Time elapsed;
//	MouseControl mContrM;
//	StateManager(RenderWindow& window) :window(window)
//	{
//		mContrM.init();
//	}
//	~StateManager()
//	{
//		LOG("deleting director", Logger::Type::Info);
//		for (auto it = gameStates.begin(); it != gameStates.end(); it++)
//			delete (*it);
//		gameStates.clear();
//	}
//
//	void run(GameState* gameState)
//	{
//		pushState(gameState);
//		running = true;
//
//		sf::Event event;
//
//		clock.restart();
//
//		while (running)
//		{
//			if (!shouldPop)
//			{
//				//handle
//				while (window.pollEvent(event))
//				{
//					if (event.type == sf::Event::Closed)
//						window.close();
//				}
//				//update
//				//accumulator += frameClock.getElapsedTime();
//				
//				elapsed = clock.restart();
//				mContrM.mousePosCords(window);
//				//mContrM.updateCursor();
//				update();
//
//				//draw
//				window.clear();
//				StateManager *sm = this;
//				window.draw(*sm);
//				window.display();
//			}
//			else
//			{
//				doPop();
//				clock.restart();
//			}
//		}
//	}
//
//	void popState()
//	{
//		shouldPop = true;
//	}
//
//	void replaceState(GameState* gameState)
//	{
//		LOG("Replacing gamestate", Logger::Type::Info);
//		LOG("States: " + gameStates.size(), Logger::Type::Info);
//		popState();
//		// store and init the new state
//		gameState->setManager(this);
//		gameState->setWindow(&window);
//		gameStates.insert(gameStates.end() - 1, gameState);
//		LOG("States in query: " + gameStates.size(), Logger::Type::Info);
//	}
//
//	void pushState(GameState* gameState)
//	{
//		LOG("Pushing state", Logger::Type::Info);
//		LOG("States: " + gameStates.size(), Logger::Type::Info);
//		gameState->setManager(this);
//		gameState->setWindow(&window);
//
//		// pause current state
//		if (!gameStates.empty())
//			gameStates.back()->onPause();
//
//		// store and init the new state
//		gameStates.push_back(gameState);
//		LOG("States in query: " + gameStates.size(), Logger::Type::Info);
//	}
//
//
//protected:
//	String name;
//
//	String getName()
//	{
//		return name;
//	}
//private:
//	RenderWindow& window;
//	vector<GameState*> gameStates;
//	Clock clock;
//
//	bool shouldPop = false, running = true;
//	void doPop()
//	{
//		LOG("Popping state", Logger::Type::Info);
//		LOG("States in query: " + gameStates.size(), Logger::Type::Info);
//		shouldPop = false;
//		if (!gameStates.empty())
//		{
//			LOG("Deleting curret gamestate", Logger::Type::Info);
//			delete gameStates.back();
//			LOG("Current gamestate deleted", Logger::Type::Info);
//			gameStates.pop_back();
//			if (!gameStates.empty())
//				gameStates.back()->onResume();
//			else
//			{
//				running = false;
//				window.close();
//			}
//			LOG("States in querry left: " + gameStates.size(), Logger::Type::Info);
//		}
//	}
//
//
//
//	void update()
//	{
//		if (!gameStates.empty())
//			gameStates.back()->onUpdate();
//	}
//
//	void draw(sf::RenderTarget& window, sf::RenderStates states) const
//	{
//		if (!gameStates.empty())
//		{
//			for (auto& it = gameStates.begin(); it != gameStates.end() -1; it++)
//			{
//				GameState *b = *it;
//				window.draw(*b);
//			}
//			GameState *b = gameStates.back();
//			window.draw(*b);
//			
//			window.draw(mContrM.cursor);
//		}
//		
//
//	}
//
//
//
//};














bool startGame(RenderWindow& window)
{
	//create map loader and load map
	tmx::MapLoader ml("/");
	ml.Load("TestMap.tmx");
	ml.AddSearchPath("Resources/images/");

	//create a box2D world
	b2World world(tmx::SfToBoxVec(sf::Vector2f(0.f, 1000.f)));

	//parse map objects
	menuNum = 1;
	thread menu(menuState,ref(window));


	const vector<MapLayer>& layers = ml.GetLayers();

	vector<PEntity*> eVec;

	int intit = -1;
	for (const auto& l : layers)
	{
		for (auto& o: l.objects)
		{
			intit++;

			if (o.GetName() == "Player")
			{
				eVec.push_back(new Player("Ring",o,world));
			}
			else
			{
				if (o.GetName() != "Image")
				eVec.push_back(new Slider("Border", o, world));
			}

		}
	}






	std::vector<std::unique_ptr<sf::Shape>> debugBoxesD;
	std::vector<DebugShape> debugShapesD;
	std::map<b2Body*, sf::CircleShape> dynamicShapesD; //we can use raw pointers because box2D manages its own memory

	if (debugMode)
	{
		const std::vector<tmx::MapLayer>& layersD = ml.GetLayers();

		for (const auto& l : layersD)
		{
			if (l.name == "Static") //static bodies which make up the map geometry
			{
				for (const auto& o : l.objects)
				{
					//receive a pointer to the newly created body
					b2Body* b = tmx::BodyCreator::Add(o, world);



					//iterate over body info to create some visual debugging shapes to help visualise
					debugBoxesD.push_back(std::unique_ptr<sf::RectangleShape>(new sf::RectangleShape(sf::Vector2f(6.f, 6.f))));
					sf::Vector2f pos = o.GetPosition();
					debugBoxesD.back()->setPosition(pos);
					debugBoxesD.back()->setOrigin(3.f, 3.f);

					for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext())
					{
						b2Shape::Type shapeType = f->GetType();
						if (shapeType == b2Shape::e_polygon)
						{
							DebugShape ds;
							ds.setPosition(pos);
							b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();

							int count = ps->GetVertexCount();
							for (int i = 0; i < count; i++)
								ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(i)), sf::Color::Green));

							ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0)), sf::Color::Green));
							debugShapesD.push_back(ds);
						}
						else if (shapeType == b2Shape::e_circle)
						{
							b2CircleShape* cs = static_cast<b2CircleShape*>(f->GetShape());
							float radius = tmx::BoxToSfFloat(cs->m_radius);
							std::unique_ptr<sf::CircleShape> c(new sf::CircleShape(radius));
							c->setPosition(pos);
							c->setOrigin(radius, radius);
							c->setOutlineColor(sf::Color::Green);
							c->setOutlineThickness(-1.f);
							c->setFillColor(sf::Color::Transparent);
							debugBoxesD.push_back(std::move(c));
						}
					}
				}
			}
		}
	}


	MouseControl mContr;
	mContr.init();




	if (menu.joinable())
	{
		menu.join();

	}


	sf::Clock clock;
	while (window.isOpen())
	{
		//poll input
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		
		mContr.mousePosCords(window);

			if (Keyboard::isKeyPressed(Keyboard::F1)) return true;
			if (Keyboard::isKeyPressed(Keyboard::Escape))
			{
				isMenu = true;
				menuNum = 1;
				if (menu.joinable()) menu.join();

			}


		

		Time elapsed = clock.restart();
		if (isMenu == false)
		{
			world.Step(1/60.f,3,8);
		}
		



		if (window.isOpen())
		{
		window.clear();

		window.draw(ml);
		}




		for (int i = 0; i < eVec.size(); i++)
		{
			if (!eVec[i]->debugBoxes.empty())
			{
				for (int j = 0; j < eVec[i]->debugBoxes.size(); j++)
				{
					window.draw(*eVec[i]->debugBoxes[j]);
				}
			}
			else if(!eVec[i]->debugShapes.empty())
			{
				for (int j = 0; j < eVec[i]->debugShapes.size(); j++)
				{
					window.draw(eVec[i]->debugShapes[j]);
				}
			}
		}
			

		if (isMenu == false)
		for (int i = 0; i < eVec.size(); i++)
		{
			
			PEntity *b = eVec[i];
			//eVec[i]->update(elapsed);
			b->update(elapsed);
		}
		if (window.isOpen())
		{
			for (int i = 0; i < eVec.size(); i++)
			{
				PEntity *b = eVec[i];
				//eVec[i]->update(elapsed);
				window.draw(*b);
			}
			if (debugMode)
			{
				for (const auto& s : debugBoxesD)
					window.draw(*s);
				for (const auto& s : debugShapesD)
					window.draw(s);
			}
		}












		if (Keyboard::isKeyPressed(Keyboard::Up))
		{
			offset += Vector2f(0, -20);
		}
		if (Keyboard::isKeyPressed(Keyboard::Down))
		{
			offset += Vector2f(0, 20);
		}
		if (Keyboard::isKeyPressed(Keyboard::Left))
		{
			offset += Vector2f(-20, 0);
		}
		if (Keyboard::isKeyPressed(Keyboard::Right))
		{
			offset += Vector2f(20, 0);
		}
		if (Keyboard::isKeyPressed(Keyboard::Num7))
		{
			viewRect += Vector2f(100, 100);
			view.setSize(viewRect);
		}
		if (Keyboard::isKeyPressed(Keyboard::Num1))
		{
			viewRect -= Vector2f(-100, -100);
			view.setSize(viewRect);
		}
		if (Keyboard::isKeyPressed(Keyboard::Num0))
		{
			viewRect = Vector2f(1400, 1000);
			view.setSize(viewRect);
		}
		RectangleShape itnotdie;
		if (itnotdie.getPosition().y >= 300) itnotdie.setPosition(25, 25);
		itnotdie.setPosition(25, itnotdie.getPosition().y + 10);

		if (window.isOpen())
		{
		window.draw(itnotdie);

		window.draw(mContr.pointer);


		setCoordinateForView(offset.x,offset.y);

		window.setView(view);

		window.display();
		}

	}

	return false;
}



bool gameRestart(RenderWindow& window)
{
	while (true)
	{
		if (startGame(window))
		{

		}
		else
		{
			return false;
		}
	}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////







//class GameActionState :public GameState
//{
//private:
//
//	vector<PEntity*> eVec;
//	tmx::MapLoader ml;
//	vector<MapLayer> layers;
//	b2World world;
//	std::vector<std::unique_ptr<sf::Shape>> debugBoxesD;
//	std::vector<DebugShape> debugShapesD;
//	std::map<b2Body*, sf::CircleShape> dynamicShapesD; //we can use raw pointers because box2D manages its own memory
//public:
//
//
//	GameActionState(RenderWindow* window, StateManager* manager) :GameState(window, manager), ml("/"), world(tmx::SfToBoxVec(sf::Vector2f(0.f, 1000.f)))
//	{
//		//create map loader and load map
//		
//		ml.AddSearchPath("Resources/images/");
//		ml.Load("TestMap.tmx");
//
//
//		//create a box2D world
//
//
//		//parse map objects
//		menuNum = 1;
//
//
//		Image borderI, playerI;
//		borderI.loadFromFile("border.png");
//		playerI.loadFromFile("Resources/images/Ring32.png");
//
//		layers = ml.GetLayers();
//
//		int intit = -1;
//		for (const auto& l : layers)
//		{
//			for (auto& o : l.objects)
//			{
//				intit++;
//
//				if (o.GetName() == "Player")
//				{
//					eVec.push_back(new Player(playerI, "Ring", o, world));
//				}
//				else
//				{
//					if (o.GetName() != "Image")
//						eVec.push_back(new Slider(borderI, "Border", o, world));
//				}
//
//			}
//		}
//
//
//
//
//
//
//
//
//		if (debugMode)
//		{
//			const std::vector<tmx::MapLayer>& layersD = ml.GetLayers();
//
//			for (const auto& l : layersD)
//			{
//				if (l.name == "Static") //static bodies which make up the map geometry
//				{
//					for (const auto& o : l.objects)
//					{
//						//receive a pointer to the newly created body
//						b2Body* b = tmx::BodyCreator::Add(o, world);
//
//
//
//						//iterate over body info to create some visual debugging shapes to help visualise
//						debugBoxesD.push_back(std::unique_ptr<sf::RectangleShape>(new sf::RectangleShape(sf::Vector2f(6.f, 6.f))));
//						sf::Vector2f pos = o.GetPosition();
//						debugBoxesD.back()->setPosition(pos);
//						debugBoxesD.back()->setOrigin(3.f, 3.f);
//
//						for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext())
//						{
//							b2Shape::Type shapeType = f->GetType();
//							if (shapeType == b2Shape::e_polygon)
//							{
//								DebugShape ds;
//								ds.setPosition(pos);
//								b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();
//
//								int count = ps->GetVertexCount();
//								for (int i = 0; i < count; i++)
//									ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(i)), sf::Color::Green));
//
//								ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0)), sf::Color::Green));
//								debugShapesD.push_back(ds);
//							}
//							else if (shapeType == b2Shape::e_circle)
//							{
//								b2CircleShape* cs = static_cast<b2CircleShape*>(f->GetShape());
//								float radius = tmx::BoxToSfFloat(cs->m_radius);
//								std::unique_ptr<sf::CircleShape> c(new sf::CircleShape(radius));
//								c->setPosition(pos);
//								c->setOrigin(radius, radius);
//								c->setOutlineColor(sf::Color::Green);
//								c->setOutlineThickness(-1.f);
//								c->setFillColor(sf::Color::Transparent);
//								debugBoxesD.push_back(std::move(c));
//							}
//						}
//					}
//				}
//			}
//		}
//
//	}
//
//	void  onUpdate()
//	{
//		sf::Event event;
//		while (window->pollEvent(event))
//		{
//			if (event.type == sf::Event::Closed)
//				window->close();
//
//
//		}
//			if (Keyboard::isKeyPressed(Keyboard::F1)) {}
//			if (Keyboard::isKeyPressed(Keyboard::F1))
//			{
//				isMenu = true;
//				menuNum = 1;
//				manager->popState(/*new GameMenuState(window, manager)*/);
//
//			}
//
//
//
//
//		Time elapsed = manager->elapsed;
//		if (isMenu == false)
//		{
//			world.Step(1 / 60.f, 3, 8);
//		}
//
//
//		if (isMenu == false)
//			for (int i = 0; i < eVec.size(); i++)
//			{
//
//				PEntity *b = eVec[i];
//				//eVec[i]->update(elapsed);
//				b->update(elapsed);
//			}
//
//		if (Keyboard::isKeyPressed(Keyboard::Up))
//		{
//			offset += Vector2f(0, -20);
//		}
//		if (Keyboard::isKeyPressed(Keyboard::Down))
//		{
//			offset += Vector2f(0, 20);
//		}
//		if (Keyboard::isKeyPressed(Keyboard::Left))
//		{
//			offset += Vector2f(-20, 0);
//		}
//		if (Keyboard::isKeyPressed(Keyboard::Right))
//		{
//			offset += Vector2f(20, 0);
//		}
//		if (Keyboard::isKeyPressed(Keyboard::Num7))
//		{
//			viewRect += Vector2f(100, 100);
//			view.setSize(viewRect);
//		}
//		if (Keyboard::isKeyPressed(Keyboard::Num1))
//		{
//			viewRect -= Vector2f(-100, -100);
//			view.setSize(viewRect);
//		}
//		if (Keyboard::isKeyPressed(Keyboard::Num0))
//		{
//			viewRect = Vector2f(1400, 1000);
//			view.setSize(viewRect);
//		}
//
//			setCoordinateForView(offset.x, offset.y);
//		
//	}
//
//	void onPause()
//	{
//		isMenu = true;
//	}
//	void onResume()
//	{
//		isMenu = false;
//	}
//
//
//private:
//	virtual void draw(RenderTarget& window, RenderStates state) const
//	{
//		window.draw(ml);
//
//		for (int i = 0; i < eVec.size(); i++)
//		{
//			if (!eVec[i]->debugBoxes.empty())
//			{
//				for (int j = 0; j < eVec[i]->debugBoxes.size(); j++)
//				{
//					window.draw(*eVec[i]->debugBoxes[j]);
//				}
//			}
//			else if (!eVec[i]->debugShapes.empty())
//			{
//				for (int j = 0; j < eVec[i]->debugShapes.size(); j++)
//				{
//					window.draw(eVec[i]->debugShapes[j]);
//				}
//			}
//		}
//
//		for (int i = 0; i < eVec.size(); i++)
//		{
//			PEntity *b = eVec[i];
//			//eVec[i]->update(elapsed);
//			window.draw(*b);
//		}
//		if (debugMode)
//		{
//			for (const auto& s : debugBoxesD)
//				window.draw(*s);
//			for (const auto& s : debugShapesD)
//				window.draw(s);
//		}
//
//		window.draw(manager->mContrM.pointer);
//
//		window.setView(view);
//
//	}
//
//};





//class GameMenuState :public GameState
//{		
//private:
//	Texture menuStartT, menuControlT, menuControlTS, menuExitT, menuExitTS, aboutTexture, solidMenuT, menuStartTS, menuExitTScontrolTextureT;
//	bool mouseOver = false;
//	int selector;
//public:
//		Sprite menuStart, menuControl, menuExit, about, solidMenu;
//		MouseControl mContr;
//
//	GameMenuState(RenderWindow* window, StateManager* manager) :GameState(window, manager)
//	{
//
//		isMenu = true; cout << "MenuMode enabled" << endl;
//
//		
//
//		menuStartT.loadFromFile("Resources/images/menu/SolidBackToGameNS.png");
//		menuControlT.loadFromFile("Resources/images/menu/ControlsNS.png");
//		menuExitT.loadFromFile("Resources/images/menu/SolidExitNS.png");
//
//		aboutTexture.loadFromFile("Resources/images/menu/SolidBackToGameNS.png");
//
//		menuStartTS.loadFromFile("Resources/images/menu/SolidBackToGameSel.png");
//		menuExitTS.loadFromFile("Resources/images/menu/SolidExitSel.png");
//		menuControlTS.loadFromFile("Resources/images/menu/ControlsSel.png");
//
//		solidMenuT.loadFromFile("Resources/images/menu/SolidMenuEmpty.png");
//
//		menuStart.setTexture(menuStartT); menuExit.setTexture(menuExitT); solidMenu.setTexture(solidMenuT);
//
//
//		menuStart.setPosition(200 + offset.x, 150 + offset.y);
//		menuControl.setPosition(250 + offset.x, 200 + offset.y);
//		menuExit.setPosition(230 + offset.x, 250 + offset.y);
//		solidMenu.setPosition(70 + offset.x, 0 + offset.y);
//
//		
//
//		mContr.init();
//
//	}
//
//	void  onUpdate()
//	{
//		sf::Event event;
//		while (window->pollEvent(event))
//		{
//			if (event.type == sf::Event::Closed)
//				window->close();
//
//
//		}
//			//if (Keyboard::isKeyPressed(Keyboard::F1)) {}
//			//if (Keyboard::isKeyPressed(Keyboard::Escape))
//			//{
//			//	isMenu = true;
//			//	menuNum = 1;
//			//	manager->popState(/*new GameMenuState(window, manager)*/);
//
//			//}
//
//		mouseOver = false;
//		menuNum = 1;
//		//mContr.mousePosCords(window);
//
//		
//
//		menuStart.setPosition(offset.x - 250/*200*/, offset.y - 300/*150*/);
//		menuControl.setPosition(offset.x - 250/*250*/, offset.y - 250/*200*/);
//		menuExit.setPosition(offset.x - 250, offset.y - 200);
//		solidMenu.setPosition(offset.x - 400, offset.y - 400);
//
//		menuStart.setTexture(menuStartT);
//		menuExit.setTexture(menuExitT);
//		//menuStart.setColor(Color::White);
//		//menuControl.setColor(Color::White);
//		//menuExit.setColor(Color::White);
//		
//
//
//
//
//		if (menuNum == 1)
//		{
//			if (menuStart.getGlobalBounds().intersects(manager->mContrM.cursor.getGlobalBounds())) { /*menuStart.setColor(Color::Blue);*/ selector = 1; /*gameRunning();*/ mouseOver = true; menuStart.setTexture(menuStartTS); }
//			if (menuControl.getGlobalBounds().intersects(manager->mContrM.cursor.getGlobalBounds())) { /*menu2.setColor(Color::Blue); menuStart.setTexture(menuStartT); menuExit.setTexture(menuExitT);*/ selector = 2; mouseOver = true; }
//			if (menuExit.getGlobalBounds().intersects(manager->mContrM.cursor.getGlobalBounds())) {/* menuExit.setColor(Color::Blue);*/ selector = 3; mouseOver = true;  menuExit.setTexture(menuExitTS); }
//		}
//
//
//
//		if (mContr.mouseClickLeft() && mouseOver == true)
//		{
//			if (selector == 1) { isMenu = false;  manager->pushState(new GameActionState(window,manager)); }//���� ������ ������ ������, �� ������� �� ���� 
//			if (selector == 3) { manager->popState(); window->close(); isMenu = false; }
//		}
//
//		if (Keyboard::isKeyPressed(Keyboard::Escape))
//		{
//			window->close();
//			manager->popState();
//		}
//	}
//
//	void onPause() 
//	{
//		isMenu = false;
//	}
//	void onResume()
//	{
//		isMenu = true;
//	}
//
//
//private:
//	virtual void draw(RenderTarget& window, RenderStates state) const 
//	{
//		if (isMenu)
//		{
//		window.draw(solidMenu);
//		window.draw(menuStart);
//		window.draw(menuExit);
//
//		window.draw(manager->mContrM.pointer);
//		}
//
//		
//	}
//
//};

/*
class GameState :Drawable
{

friend class StateManager;

public:


GameState()
{
//ctr
}
~GameState()
{
//dtr
}

virtual void onUpdate() = 0;



virtual void onPause() = 0;

virtual void onResume() = 0;

protected:

StateManager* manager;
void setManager(StateManager* manager)
{
this->manager = manager;
}


RenderWindow* window;
void setWindow(RenderWindow* window)
{
this->window = window;
}


private:
virtual void draw(RenderTarget& window, RenderStates state) const {}


};


*/






int main()
{
	RenderWindow window(VideoMode(1400, 1000), "Test!");
	window.setFramerateLimit(59);
	offset = Vector2f(1400 / 2, 1000 / 2);
	txt.loadFromFile("Resources/font/Font.TTF");

	view.setSize(Vector2f(1400, 1000));
	StateManager sm(window);

	rm->setAudioDir("Resources/audio/");
	rm->setFontDir("Resources/font/");
	rm->setTextureDir("Resources/images/");

	text.setFont(txt/*rm->getFont("arial.ttf")*/);
	text.setCharacterSize(20);
	text.setColor(Color::White);

	sm.run(new GameMenuState(&window, &sm));
	//gameRestart(window);
	//gameRunning(window);
	return 0;
	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		offset += Vector2f(0, -20);
	}
	if (Keyboard::isKeyPressed(Keyboard::Down))
	{
		offset += Vector2f(0, 20);
	}
	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		offset += Vector2f(-20, 0);
	}
	if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		offset += Vector2f(20, 0);
	}
	if (Keyboard::isKeyPressed(Keyboard::Num1))
	{
		viewRect += Vector2f(140, 100);
		view.setSize(viewRect);
	}
	if (Keyboard::isKeyPressed(Keyboard::Num9))
	{
		viewRect = viewRect - Vector2f(100, 100);
		view.setSize(viewRect);
	}
	if (Keyboard::isKeyPressed(Keyboard::Num0))
	{
		viewRect = Vector2f(1400, 1000);
		view.setSize(viewRect);
	}
	RectangleShape itnotdie;
	if (itnotdie.getPosition().y >= 300) itnotdie.setPosition(25, 25);
	itnotdie.setPosition(25, itnotdie.getPosition().y + 10);
	window.draw(itnotdie);
}


