#ifndef GAMEACTIONSTATE_H_
#define GAMEACTIONSTATE_H_

#include "StateManager.h"
#include "GameState.h"
#include <vector>
//#include "view.h"
//#include "global.h"
#include <tmx\MapLoader.h>
#include <SFML\Graphics.hpp>
#include "ResourceManager.h"
#include "MouseControl.h"
#include "EntityList.h"


extern int menuNum;
extern bool isMenu;
extern bool debugMode;



using namespace sf;
using namespace tmx;

extern View view;
extern Vector2f offset;
extern Vector2f viewRect;
extern void setCoordinateForView(float x, float y);



class GameMenuState;
class GameActionState :public GameState
{
private:
	tmx::MapLoader ml;
	vector<PEntity*> eVec;

	vector<MapLayer> layers;
	b2World world;
	std::vector<std::unique_ptr<sf::Shape>> debugBoxesD;
	std::vector<DebugShape> debugShapesD;
	std::map<b2Body*, sf::CircleShape> dynamicShapesD; //we can use raw pointers because box2D manages its own memory
public:


	GameActionState(RenderWindow* window, StateManager* manager);
	~GameActionState();

	void  onUpdate();


	void onPause();

	void onResume();


private:
	virtual void draw(RenderTarget& window, RenderStates state) const;

};
#endif // !GAMEACTIONSTATE_H_