//#pragma once
#ifndef GAMESTATE_H_
#define GAMESTATE_H_
#include <SFML\Graphics.hpp>

using namespace sf;

class GameState :public Drawable
{

	friend class StateManager;

public:


	GameState(RenderWindow* window, StateManager* manager);
	~GameState();

	/// \breif Called while this gamestate this higest priority
	/// \breif ���������� ����: �������� ������� ������� ��������� (����� ������������)
	virtual void onUpdate();

	/// \breif  Called then this gamestate now less priority
	/// \breif  ���������� ����� ������ ������� ���������� ������������ ��������
	virtual void onPause();

	/// \breif Called then this gamestate returned to hightest priority
	/// \breif ���������� ����� ������ ������� ���������� ����� ������������
	virtual void onResume();

protected:

	StateManager* manager;
	void setManager(StateManager* manager);


	RenderWindow* window;
	void setWindow(RenderWindow* window);


private:
	/// \brief Called evry time step; Layer referenced to priority
	/// \brief ���������� ������ ���; ���� ��������� ������� �� ���������� ����� ��������
	virtual void draw(RenderTarget& window, RenderStates state) const = 0;


};
#endif // !GAMESTATE_H_