
#pragma once

#include <SFML/Graphics.hpp>
//#include "Game.h"
using namespace sf;


void menu(RenderWindow & window,bool reLoadLvl, int curMenu) {
	Texture menuStartT, menuControlT, menuControlTS, menuExitT, menuExitTS, aboutTexture, solidMenuT, menuStartTS, menuExitTScontrolTextureT;
	
	View view;
	view.reset(FloatRect(0, 0, 800, 600));
	bool isMenu = true;
	int menuNum;
	menuStartT.loadFromFile("Resources/images/menu/SolidBackToGameNS.png");
	menuControlT.loadFromFile("Resources/images/menu/ControlsNS.png");
	menuExitT.loadFromFile("Resources/images/menu/SolidExitNS.png");

	aboutTexture.loadFromFile("Resources/images/menu/SolidBackToGameNS.png");

	menuStartTS.loadFromFile("Resources/images/menu/SolidBackToGameSel.png");
	menuExitTS.loadFromFile("Resources/images/menu/SolidExitSel.png");
	menuControlTS.loadFromFile("Resources/images/menu/ControlsSel.png");

	solidMenuT.loadFromFile("Resources/images/menu/SolidMenuEmpty.png");


	Sprite menuStart(menuStartT), menuControl(menuControlT), menuExit(menuExitT), about(aboutTexture), solidMenu(solidMenuT);
	//bool isMenu = 1;
	//int menuNum = 0;
	menuStart.setPosition(200, 150);
	menuControl.setPosition(250, 200);
	menuExit.setPosition(230, 250);
	solidMenu.setPosition(70, 0);

	//////////////////////////////����///////////////////
	while (isMenu && window.isOpen())
	{
		
		//
		//Event event;
		//while (window.pollEvent(event))
		//{
		//	if (event.type == sf::Event::Closed)
		//		window.close();
		//}

		menuStart.setPosition(200, 150);
		menuControl.setPosition(250, 200);
		menuExit.setPosition(230, 250);
		solidMenu.setPosition(70, 0);
		
		menuStart.setColor(Color::White);
		menuControl.setColor(Color::White);
		menuExit.setColor(Color::White);
		menuNum = 0;
		window.clear(Color(129, 181, 221));

		if (curMenu == 1) {

			if (IntRect(200, 150, 300, 50).contains(Mouse::getPosition(window))) { /*menuStart.setColor(Color::Blue);*/ menuNum = 1; /*gameRunning();*/ }
			if (IntRect(250, 200, 300, 50).contains(Mouse::getPosition(window))) { /*menu2.setColor(Color::Blue); menuStart.setTexture(menuStartT); menuExit.setTexture(menuExitT);*/ menuNum = 2; }
			if (IntRect(230, 250, 300, 50).contains(Mouse::getPosition(window))) {/* menuExit.setColor(Color::Blue);*/ menuNum = 3; }

			if (Keyboard::isKeyPressed(Keyboard::W) || Keyboard::isKeyPressed(Keyboard::Up))
			{
				menuNum = menuNum + 1;
				if (menuNum >= 4)
				{
					menuNum -= 3;
				}

			}
			if (Keyboard::isKeyPressed(Keyboard::S) || Keyboard::isKeyPressed(Keyboard::Down))
			{
				menuNum = menuNum - 1;
				if (menuNum <= -1)
				{
					menuNum += 4;
				}

			}
			if (menuNum == 3){
				menuStart.setTexture(menuStartT); menuExit.setTexture(menuExitTS); menuControl.setTexture(menuControlT);
			}
			if (menuNum == 2) {
				//menuStart.setTexture(menuStartT); menuExit.setTexture(menuExitT); menuControl.setTexture(menuControlTS);
			}
			if (menuNum == 1) {
				menuStart.setTexture(menuStartTS); menuExit.setTexture(menuExitT); menuControl.setTexture(menuControlT);
			} 
		}


			if (Mouse::isButtonPressed(Mouse::Left)|| Keyboard::isKeyPressed(Keyboard::Return))
			{
				if (menuNum == 1) { isMenu = false; reLoadLvl == true; }//���� ������ ������ ������, �� ������� �� ���� 
				//if (menuNum == 2) { window.draw(about); window.display(); while (!Keyboard::isKeyPressed(Keyboard::Escape)); }
				if (menuNum == 3) { window.close(); isMenu = false; }

			}
		

		window.setView(view);

		window.draw(solidMenu);
		window.draw(menuStart);
		//window.draw(menuControl);
		window.draw(menuExit);

		window.display();

		if (Keyboard::isKeyPressed(Keyboard::Escape) && menuNum == 0) { menuNum == 3; window.close(); isMenu = false; }

	}
	////////////////////////////////////////////////////
};
