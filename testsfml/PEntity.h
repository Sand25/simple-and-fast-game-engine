#ifndef PENTITY_H_
#define PENTITY_H_
#include <SFML\Graphics.hpp>
#include <tmx\MapLoader.h>
//#include "StateManagment.h"
#include "MouseControl.h"
#include <vector>
#include <tmx/tmx2box2d.h>
#include <Box2D/Box2D.h>
#include "ResourceManager.h"

using namespace sf;
using namespace tmx;





class PEntity :public Drawable, public Transformable {
protected:

public:

	float dx, dy, x, y, rotAngle, speed, moveTimer, tdx, tdy, tx, ty, velocity;
	int w, h, health;
	bool life, isMove, onGround, canMove;
	Sprite sprite;
	String name;
	Vector2f curPos, targetPos, posTmp, velocityV, directionV;

	b2Body* me;
	b2World& world;

	Vertex vert;

	CircleShape circle;


	std::vector<std::unique_ptr<sf::Shape>> debugBoxes;
	std::vector<DebugShape> debugShapes;

	PEntity(String Name, tmx::MapObject o, b2World &world) :world(world)
	{
		x = o.GetPosition().x; y = o.GetPosition().y; w = o.GetAABB().height; h = o.GetAABB().width; name = Name; moveTimer = 0;
		speed = 0; health = 100; dx = 0; dy = 0;
		life = true; onGround = false; isMove = false;
		//sprite.setTexture(texture);







		if (o.GetShapeType() == Circle)
		{
			if (w <= 32)
			{
				sprite.setOrigin(w / 2, h / 2);
			}

		}

		//me = BodyCreator::Add(o, world);
		//debugBorder();
	}

	~PEntity()
	{
		me->DestroyFixture(me->GetFixtureList());
		points.clear();
	}

	FloatRect getRect()
	{
		return FloatRect(x, y, w, h);
	}



	virtual void update(Time time) = 0;



	bool isRectangle(vector<Vector2f> tester)
	{
		int iter;


		if (tester.size() != 4)
		{
			std::cout << "Error: unexpected loop in entity class loader" << std::endl;
		}
		else
		{
			float x1, y1, x2, y2, x3, y3, x4, y4;
			x1 = tester[0].x; y1 = tester[0].y;
			x2 = tester[1].x; y2 = tester[1].y;
			x3 = tester[2].x; y3 = tester[2].x;
			x4 = tester[3].x; y4 = tester[3].x;
			cout << "X =" << x1 << " " << x2 << " " << x3 << " " << x4 << endl;
			cout << "y =" << y1 << " " << y2 << " " << y3 << " " << y4 << endl;
			if (x1 == x4 && x2 == x3)
			{
				if (y1 == y2 && y3 == -y4)
				{
					cout << "sucses" << endl;
					return true;
				}
			}
		}
		cout << "failure" << endl;
		return false;
	}


	void debugBorder()
	{

		if (!debugBoxes.empty())
		{
			for (int i = 0; i < debugBoxes.size(); i++)
			{
				debugBoxes.erase(debugBoxes.begin() + i);

			}
			debugBoxes.clear();
		}
		if (!debugShapes.empty())
		{
			//for (auto& itr = debugShapes.begin(); itr != debugShapes.end(); itr++)
			//{
			//
			//debugShapes.erase(itr);

			//}
			debugShapes.clear();
		}

		debugBoxes.push_back(std::unique_ptr<sf::RectangleShape>(new sf::RectangleShape(sf::Vector2f(6.f, 6.f))));
		sf::Vector2f pos = tmx::BoxToSfVec(me->GetPosition());
		debugBoxes.back()->setPosition(pos);
		debugBoxes.back()->setOrigin(3.f, 3.f);

		for (b2Fixture* f = me->GetFixtureList(); f; f = f->GetNext())
		{
			b2Shape::Type shapeType = f->GetType();
			if (shapeType == b2Shape::e_polygon)
			{
				DebugShape ds;
				ds.setPosition(pos);
				b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();

				int count = ps->GetVertexCount();
				for (int i = 0; i < count; i++)
					ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(i)), sf::Color::Green));

				ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0)), sf::Color::Green));
				debugShapes.push_back(ds);
			}
			else if (shapeType == b2Shape::e_circle)
			{
				b2CircleShape* cs = static_cast<b2CircleShape*>(f->GetShape());
				float radius = tmx::BoxToSfFloat(cs->m_radius);
				std::unique_ptr<sf::CircleShape> c(new sf::CircleShape(radius));
				c->setPosition(pos);
				c->setOrigin(radius, radius);
				c->setOutlineColor(sf::Color::Green);
				c->setOutlineThickness(-1.f);
				c->setFillColor(sf::Color::Transparent);
				debugBoxes.push_back(std::move(c));
			}
		}
	}


private:

	std::vector<sf::Vector2f> points;
	sf::Vector2f pos;
	std::vector<sf::Vertex> vertices;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		// apply the transform
		states.transform *= getTransform();
		//states.texture = &texture;

		// our particles don't use a texture
		//states.texture = NULL;

		// draw the vertex array
		target.draw(sprite, states);


	}


};
#endif