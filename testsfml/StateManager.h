#ifndef STATEMANAGER_H_
#define STATEMANAGER_H_
#include <SFML\Graphics.hpp>
#include "MouseControl.h"
#include <tmx\Log.h>
#include "GameState.h"
#include "ResourceManager.h"

using namespace tmx;
using namespace sf;

class StateManager :public Drawable
{
public:
	Time elapsed;
	MouseControl mContrM;
	StateManager(RenderWindow& window);
	~StateManager();

	void run(GameState* gameState);


	void popState();

	void replaceState(GameState* gameState);


	void pushState(GameState* gameState);

	void gameClose();

	void clearQuery(GameState* state);




protected:
	String name;

	String getName();

private:
	RenderWindow& window;
	vector<GameState*> gameStates;
	Clock clock;

	bool shouldPop = false, running = true;
	void doPop();




	void update();


	void draw(sf::RenderTarget& window, sf::RenderStates states) const;




};
#endif
