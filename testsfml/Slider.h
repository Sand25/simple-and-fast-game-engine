#pragma once
#include "PEntity.h"




class Slider :public PEntity
{
public:
	Slider(String Name, MapObject o, b2World& world) :PEntity(Name, o, world)
	{
		//obj = lev.GetAllObjects();

		me = BodyCreator::Add(o, world);
		sprite.setTexture(rm->getTexture("border.png"));

		b2Fixture* f = me->GetFixtureList();
		b2Shape::Type shapeType = f->GetType();
		Vector2f pos = o.GetPosition();


		if (shapeType == b2Shape::e_polygon)
		{

			b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();
			int count = ps->GetVertexCount();
			int iter;





			if (count == 4)
			{
				vector<Vector2f> tester;
				for (iter = 0; iter < count; iter++)
				{
					cout << "isRect. x= " << BoxToSfVec(ps->GetVertex(iter)).x << " y = " << BoxToSfVec(ps->GetVertex(iter)).y << " h= " << h << " w = " << w << endl;
					tester.push_back(tmx::BoxToSfVec(ps->GetVertex(iter)));
				}

				isRect = isRectangle(tester);

			}
			if (!isRect)
			{
				for (iter = 0; iter < count; iter++)
					vertices.push_back(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(iter))));

				vertices.push_back(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0))));
				/*vertices[iter+1].position = pos;*/
				for (iter = 0; iter < vertices.size(); iter++)
				{
					vertices[iter].position = Vector2f(vertices[iter].position.x + pos.x, vertices[iter].position.y + pos.y);
				}
			}

		}
		if (o.GetName() == "Circle" || o.GetName() == "circle")
		{
			cout << "Circle" << endl;
			float radius = h / 2;
			circle.setRadius(radius);
			circle.setPosition(pos);
			//circle.setOrigin(radius, radius);
			circle.setOutlineColor(sf::Color::Green);
			circle.setOutlineThickness(-1.f);
			circle.setTexture(new Texture(rm->getTexture("border.png")));
		}
		if (isRect == true)
		{
			//cout << "isRect. x= " <<x <<" y = "<< y <<" h= "<<h <<" w = "<<w << endl;
			sprite.setOrigin(w / 2, h / 2);

		}

		x = BoxToSfFloat(me->GetPosition().x) + w / 2 - 16;
		y = -BoxToSfFloat(me->GetPosition().y) + h / 2 - 16;



		//sprite.setPosition(x,y);
		sprite.setPosition(x /*+ w / 2*/, y /*+ h / 2*/);

		sprite.setScale(h / 32, w / 32);
		//rotAngle = rotateAngle;
		//if(rotateAngle !=0)
		//sprite.setRotation(rotAngle);
	}
	void update(Time time)
	{
		x = BoxToSfFloat(me->GetPosition().x) + w / 2 - 16;
		y = -BoxToSfFloat(me->GetPosition().y) + h / 2 - 16;

		debugBorder();

		//sprite.setPosition(x,y);
		sprite.setPosition(x /*+ w / 2*/, y /*+ h / 2*/);
	}
private:

	bool isRect = false;
	std::vector<sf::Vector2f> points;
	sf::Vector2f pos;
	std::vector<sf::Vertex> vertices;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		states.texture = &rm->getTexture("border.png");
		if (!isRect)
		{
			target.draw(&vertices[0], vertices.size(), TrianglesStrip, states);
		}
		else target.draw(sprite);


		target.draw(circle);
	}
};