
#include <SFML/Graphics.hpp>
#include "iostream"

#include "MouseControl.h"

extern Vector2f viewRect;

using namespace sf;
using namespace std;

extern Vector2f curMousePos;
extern Vector2f offset;
//�������� ��� �����

//�����-�������������. ��� ����� ��������� ��� �������
	void MouseControl::init()
	{
		Texture texture;
		Image image;
		image.create(1, 1, Color::Color(0, 0, 0, 0));

		texture.loadFromImage(image);
		cursor.setTexture(texture);
		pointer.setTexture(rm->getTexture("pointer.png"));
		pointer.setScale(0.1f, 0.1f);
	}
	//����� �����. ������� � ������� ��������� ������� (bugged)
	void MouseControl::mouseCheck()
	{

		//if (!Mouse::isButtonPressed(Mouse::Left)) { isPresL = false; }
		//if (!Mouse::isButtonPressed(Mouse::Right)) { isPresR = false; }
		cout << "L " << isPresL << " R " << isPresR << endl; //������� ����� � �������
	}
	//��������� ���� ����� ������� ����. ���������� bool.
	bool MouseControl::mouseClickLeft()
	{
		if (Mouse::isButtonPressed(Mouse::Left))
		{
			if (isPresL == false)
			{
				isPresL = true;
				return true;
			}
			else return false;
		}
		else
		{
			isPresL = false;
			return false;
		}
	}
	//��������� ���� ������ ������� ����. ���������� bool.
	bool MouseControl::mouseClickRight()
	{
		if (Mouse::isButtonPressed(Mouse::Right))
		{
			if (isPresR == false)
			{
				isPresR = true;
				return true;
			}
			else return false;
		}
		else
		{
			isPresR = false;
			return false;
		}
	}
	//��������� ������ �� ����� ������ ����. ���������� bool.
	bool MouseControl::mouseDownLeft()
	{

		if (Mouse::isButtonPressed(Mouse::Left))
		{
			isPresL = true;
			return true;
		}

		else { isPresL = false; return false; }
	}
	//��������� ������ �� ������ ������ ����. ���������� bool.
	bool MouseControl::mouseDownRight()
	{

		if (Mouse::isButtonPressed(Mouse::Right))
		{
			isPresR = true;
			return true;
		}
		else { isPresR = false; return false; }
	}
	//��������� ����������� ����� ������ ����. ��������� ������ down. ���������� bool. 
	bool MouseControl::mouseUpLeft()
	{
		if (downL == true)
		{
			if (!Mouse::isButtonPressed(Mouse::Left)) { isPresL = false; return true; }
		}
		else return false;
	}
	//��������� ����������� ������ ������ ����. �������� ������ down. ���������� bool.
	bool MouseControl::mouseUpRight()
	{
		if (downL == true)
		{
			if (!Mouse::isButtonPressed(Mouse::Right)) { isPresR = false; return true; }
		}
		else return false;
	}
	//��������� ����������������� ��������� ����
	void MouseControl::mousePosCords(RenderWindow &window)
	{
		curMousePos = Vector2f(Mouse::getPosition(window));
		cursor.setPosition((Vector2f(curMousePos.x * (viewRect.x /1400), curMousePos.y * (viewRect.y/1000))) + (Vector2f((offset.x - (viewRect.x / 2)) /** (viewRect.x / 1400)*/, (offset.y - (viewRect.y/2)) /**(viewRect.y / 1000)*/)));
		pointer.setPosition(cursor.getPosition());
		mX = Mouse::getPosition(window).x;

		mY = Mouse::getPosition(window).y;

	}


	Sprite MouseControl::updateCursor()
	{
		cursor.setPosition(curMousePos + (Vector2f(offset.x - viewRect.x / 2, offset.y - viewRect.y / 2)));
		pointer.setPosition(cursor.getPosition());
		return cursor;
	}

	IntRect MouseControl::getCursorRect()
	{
		cursor.setPosition(curMousePos + (Vector2f(offset.x - viewRect.x / 2, offset.y - viewRect.y / 2)));
		pointer.setPosition(cursor.getPosition());
		return IntRect(cursor.getPosition().x, cursor.getPosition().y, 1, 1);
	}

	//IntRect getCursorRectWithOffset()
	//{
	//	cursor.setPosition(curMousePos);
	//	return IntRect(Vector2i(cursor.getPosition().x + offset.x, cursor.getPosition().y + offset.y), Vector2i(1, 1));
	//}

	//���������� Vector2i ������� �������
	Vector2i MouseControl::mousePos(RenderWindow &window)
	{

		curMousePos = Vector2f(Mouse::getPosition(window));
		cursor.setPosition(curMousePos + (Vector2f(offset.x - viewRect.x / 2, offset.y - viewRect.y / 2)));
		pointer.setPosition(cursor.getPosition());
		return Vector2i(Mouse::getPosition(window));

	}


