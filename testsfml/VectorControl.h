#pragma once
#include <SFML/Graphics.hpp>
#include "iostream"

using namespace sf;
using namespace std;

extern Vector2f curMousePos;





//������������ �������
//���������� ������
Vector2f vectorNormilize(Vector2f vector)
{
	//float inv_length = (1 / std::sqrt(((vector).x*(vector).x) + ((vector).y*(vector).y)));
	//return Vector2f(vector.x * inv_length, vector.y * inv_length);
	if (sqrt(vector.x*vector.x + vector.y*vector.y) == 0)
	{
		vector.x = 1;
		return vector;
	}
	float length = sqrt(vector.x*vector.x + vector.y*vector.y);

	vector.x /= length;
	vector.y /= length;
	return vector;
}

//������������ �������
//���������� ������
Vector2i vectorNormilize(Vector2i vector)
{
	//float inv_length = (1 / std::sqrt(((vector).x*(vector).x) + ((vector).y*(vector).y)));
	//return Vector2f(vector.x * inv_length, vector.y * inv_length);
	if (sqrt(vector.x*vector.x + vector.y*vector.y) == 0)
	{
		vector.x = 1;
		return vector;
	}
	float length = sqrt(vector.x*vector.x + vector.y*vector.y);

	vector.x /= length;
	vector.y /= length;
	return vector;
}

//������������ ������� ��� ������ ��� ������ � ���� float
//���������� ������
Vector2u vectorNormilize(Vector2u vector, float velocity)
{
	float inv_length = (1 / velocity);
	return Vector2u(vector.x * inv_length, vector.y * inv_length);
}

//������������ ������� ��� ������ ��� ������ � ���� �������
//���������� ������
Vector2f vectorNormilize(Vector2f vector, Vector2f vector2)
{

	float velocity = 1/std::sqrt((abs((vector - vector2).x * 5)*abs((vector - vector2).x) * 5) + (abs((vector - vector2).y * 5)*abs((vector - vector2).y * 5)));
	return Vector2f(vector.x * velocity, vector.y * velocity);
}
//������������ ������� ��� ������ ��� ������ � ���� �������
//���������� ������
Vector2i vectorNormilize(Vector2i vector, Vector2i vector2)
{
	float velocity = 1 / std::sqrt((abs((vector - vector2).x * 5)*abs((vector - vector2).x) * 5) + (abs((vector - vector2).y * 5)*abs((vector - vector2).y * 5)));
	return Vector2i(vector.x * velocity, vector.y * velocity);
}
//������������ ������� ��� ������ ��� ������ � ���� �������
//���������� ������
//Vector2u vectorNormilize(Vector2u vector, Vector2u velocity) //�� ��������
//{
//	return Vector2u(vector.x / velocity.x, vector.y / velocity.y);
//}


//�������� ���� ������������ ������� �������� ����� 2'�� ���������
//���������� unsigned int
unsigned int getAngle(Vector2u start, Vector2u end)
{
	return atan2((start - end).y, (start - end).x) * 180 / 3.14159265;
}

//�������� ���� ����� ����� ���������
//���������n int
//������� ������ ���� ������ ����
int getAngle(Vector2i start, Vector2i end)
{
	return atan2((start - end).y, (start - end).x) * 180 / 3.14159265;
}

//�������� ���� ����� ����� ���������
//���������n float
//������� ������ ���� ������ ����
float getAngle(Vector2f start, Vector2f end)
{
	return atan2((start - end).y, (start - end).x) * 180 / 3.14159265;
}

//������� ������� � Vector2i
Vector2i vector2ToVector2i(Vector2f vector)
{
	int x = vector.x;
	int y = vector.y;
	return Vector2i(x, y);
}
//������� ������� � Vector2i
Vector2i vector2ToVector2i(Vector2u vector)
{
	int x = vector.x;
	int y = vector.y;
	return Vector2i(x, y);
}
//������� ������� � Vector2f
Vector2f vector2ToVector2f(Vector2i vector)
{
	float x = vector.x;
	float y = vector.y;
	return Vector2f(x, y);
}
//������� ������� � Vector2f
Vector2f vector2ToVector2f(Vector2u vector)
{
	float x = vector.x;
	float y = vector.y;
	return Vector2f(x, y);
}
//������� ������� � Vector2u
Vector2u vector2ToVector2u(Vector2i vector)
{
	unsigned int x = vector.x;
	unsigned int y = vector.y;
	return Vector2u(x, y);
}
//������� ������� � Vector2u
Vector2u vector2ToVector2u(Vector2f vector)
{
	unsigned int x = vector.x;
	unsigned int y = vector.y;
	return Vector2u(x, y);
}
//�������� ��������� ������� ���� �������� 2'� ��������.
//���������� float
float getVDistance(Vector2f start, Vector2f end)
{
	return std::sqrt((abs((start - end).x)*abs((start - end).x)) + (abs((start - end).y)*abs((start - end).y)));
}
//�������� ��������� ������� ���� �������� 2'� ��������.
//���������� int
int getVDistance(Vector2i start, Vector2i end)
{
	return std::sqrt((abs((start - end).x)*abs((start - end).x)) + (abs((start - end).y)*abs((start - end).y)));
}
// �������� ��������� �������
//���������� int
int getVDistance(Vector2i vector)
{
	return std::sqrt((abs((vector).x)*abs((vector).x)) + (abs((vector).y)*abs((vector).y)));
}
// �������� ��������� �������
//���������� float
float getVDistance(Vector2f vector)
{
	return std::sqrt((abs((vector).x)*abs((vector).x)) + (abs((vector).y)*abs((vector).y)));
}