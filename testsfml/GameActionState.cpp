#include "GameActionState.h"
#include "GameMenuState.h"




GameActionState::GameActionState(RenderWindow * window, StateManager * manager) :GameState(window, manager), world(tmx::SfToBoxVec(sf::Vector2f(0.f, 1000.f)))
{
	//create map loader and load map

	ml.AddSearchPath("Resources/images/");
	ml.AddSearchPath("/");
	ml.Load("TestMap.tmx");


	//create a box2D world


	//parse map objects
	menuNum = 1;


	layers = ml.GetLayers();

	int intit = -1;
	for (const auto& l : layers)
	{
		for (auto& o : l.objects)
		{
			intit++;

			if (o.GetName() == "Player")
			{
				eVec.push_back(new Player("Ring", o, world));
			}
			else
			{
				if (o.GetName() != "Image")
					eVec.push_back(new Slider("Border", o, world));
			}

		}
	}








	if (debugMode)
	{
		const std::vector<tmx::MapLayer>& layersD = ml.GetLayers();

		for (const auto& l : layersD)
		{
			if (l.name == "Static") //static bodies which make up the map geometry
			{
				for (const auto& o : l.objects)
				{
					//receive a pointer to the newly created body
					b2Body* b = tmx::BodyCreator::Add(o, world);



					//iterate over body info to create some visual debugging shapes to help visualise
					debugBoxesD.push_back(std::unique_ptr<sf::RectangleShape>(new sf::RectangleShape(sf::Vector2f(6.f, 6.f))));
					sf::Vector2f pos = o.GetPosition();
					debugBoxesD.back()->setPosition(pos);
					debugBoxesD.back()->setOrigin(3.f, 3.f);

					for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext())
					{
						b2Shape::Type shapeType = f->GetType();
						if (shapeType == b2Shape::e_polygon)
						{
							DebugShape ds;
							ds.setPosition(pos);
							b2PolygonShape* ps = (b2PolygonShape*)f->GetShape();

							int count = ps->GetVertexCount();
							for (int i = 0; i < count; i++)
								ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(i)), sf::Color::Green));

							ds.AddVertex(sf::Vertex(tmx::BoxToSfVec(ps->GetVertex(0)), sf::Color::Green));
							debugShapesD.push_back(ds);
						}
						else if (shapeType == b2Shape::e_circle)
						{
							b2CircleShape* cs = static_cast<b2CircleShape*>(f->GetShape());
							float radius = tmx::BoxToSfFloat(cs->m_radius);
							std::unique_ptr<sf::CircleShape> c(new sf::CircleShape(radius));
							c->setPosition(pos);
							c->setOrigin(radius, radius);
							c->setOutlineColor(sf::Color::Green);
							c->setOutlineThickness(-1.f);
							c->setFillColor(sf::Color::Transparent);
							debugBoxesD.push_back(std::move(c));
						}
					}
				}
			}
		}
	}
	
}

GameActionState::~GameActionState()
{

}

void GameActionState::onUpdate()
{
	sf::Event event;
	while (window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window->close();


	}
	if (Keyboard::isKeyPressed(Keyboard::F1)) {}
	if (Keyboard::isKeyPressed(Keyboard::F1))
	{
		isMenu = true;
		menuNum = 2;
		manager->pushState(new GameMenuState(window, manager));
		//manager->popState(/*new GameMenuState(window, manager)*/);

	}

	


	Time elapsed = manager->elapsed;
	if (isMenu == false)
	{
		world.Step(manager->elapsed.asSeconds(), 3, 8);
	}


	if (isMenu == false)
		for (int i = 0; i < eVec.size(); i++)
		{

			PEntity *b = eVec[i];
			//eVec[i]->update(elapsed);
			b->update(elapsed);
		}

	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		offset += Vector2f(0, -20);
	}
	if (Keyboard::isKeyPressed(Keyboard::Down))
	{
		offset += Vector2f(0, 20);
	}
	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		offset += Vector2f(-20, 0);
	}
	if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		offset += Vector2f(20, 0);
	}
	if (Keyboard::isKeyPressed(Keyboard::Num7))
	{
		viewRect = Vector2f(viewRect.x-140, viewRect.y - 100);
		view.setSize(viewRect);
	}
	if (Keyboard::isKeyPressed(Keyboard::Num1))
	{
		viewRect -= Vector2f(-140, -100);
		view.setSize(viewRect);
	}
	if (Keyboard::isKeyPressed(Keyboard::Num0))
	{
		viewRect = Vector2f(1400, 1000);
		view.setSize(viewRect);
	}

	setCoordinateForView(offset.x, offset.y);

	manager->mContrM.mousePosCords(*window);
}

void GameActionState::onPause()
{
	isMenu = true;
}

void GameActionState::onResume()
{
	isMenu = false;
}

void GameActionState::draw(RenderTarget & window, RenderStates state) const
{
	window.draw(ml);

	for (int i = 0; i < eVec.size(); i++)
	{
		if (!eVec[i]->debugBoxes.empty())
		{
			for (int j = 0; j < eVec[i]->debugBoxes.size(); j++)
			{
				window.draw(*eVec[i]->debugBoxes[j]);
			}
		}
		else if (!eVec[i]->debugShapes.empty())
		{
			for (int j = 0; j < eVec[i]->debugShapes.size(); j++)
			{
				window.draw(eVec[i]->debugShapes[j]);
			}
		}
	}

	for (int i = 0; i < eVec.size(); i++)
	{
		PEntity *b = eVec[i];
		//eVec[i]->update(elapsed);
		window.draw(*b);
	}
	if (debugMode)
	{
		for (const auto& s : debugBoxesD)
			window.draw(*s);
		for (const auto& s : debugShapesD)
			window.draw(s);
	}

	//window.draw(manager->mContrM.pointer);

	window.setView(view);

}
